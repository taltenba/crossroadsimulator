# CrossroadSimulator
CrossroadSimulator is a program simulating the vehicle and pedestrian traffic in a four-way intersection with adaptative traffic light control system.

This project has been carried out as part of the *Operating Systems: Principles and Communication* course I attended during my studies. The goal was not to develop a realistic simulator but rather to put the operating system and concurrent programming concepts learned during the semester into practice.

![Image Imgur](https://i.imgur.com/h9d4LrN.png)

## Technologies
The main technologies used in this project are:
* C
* SDL 2
* gperf

## Getting started
### Prerequisites
* Any POSIX-compliant system with POSIX Realtime, Advanced Realtime, et Advanced Realtime Threads extensions.
* GCC
* gperf
* SDL 2

### Building
Simply run the makefile:
```sh
make
```

### Running
To execute the program a configuration file is required. A default configuration file is provided and can be edited if needed.
```sh
./crossroad_simulator sim_config
```

## Author
* ALTENBACH Thomas - @taltenba

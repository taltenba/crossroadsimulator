/**
 * \file
 * \brief 	Configuration of a crossroad simulator.
 * \date    Created:  11/12/2018
 * \date    Modified: 18/12/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#ifndef CRCONFIG_H_INCLUDED
#define CRCONFIG_H_INCLUDED

#include <unistd.h>
#include <crossroad.h>
#include <entity.h>

#define CAR_LENGTH 4.0f
#define PEDESTRIAN_LENGTH 1.81f
// TODO: allow user configuration for these constants:
#define MIN_MOVING_CAR_DISTANCE 5.0f
#define MIN_STATIONARY_CAR_DISTANCE 0.5f
#define MIN_PEDESTRIAN_DISTANCE 1.0f

/**
 * \struct CrossroadConfig
 * \brief The configuration of a crossroad simulator.
 */
typedef struct
{
	unsigned int step_period;                     /**< In ms */
	unsigned int green_light_dur;                 /**< In ms */
	unsigned int yellow_light_dur;                /**< In ms */
	unsigned int pedestrian_call_btn_latency;     /**< In ms */
	unsigned int overload_extra_light_time;       /**< In ms */
	unsigned int overloaded_way_threshold;        /**< In cars */
	float time_multiplier;                        /**< Time multiplier of the simulation */
	float max_velocities[ENTITY_TYPE_COUNT];      /**< In m/ms */
	float car_spawn_probs[WAY_COUNT];             /**< Probabilities that a car spawns per step period */
	float pedestrian_spawn_probs[WAY_COUNT];      /**< Probabilities that a pedestrian spawns per step period */
} CrossroadConfig;

/**
 * \struct CrossroadBuilder
 * \brief Structure contains the values used to build a CrossroadConfig.
 */
typedef struct
{
	unsigned int step_period;                     /**< In ms */
	unsigned int green_light_dur;                 /**< In s */
	unsigned int yellow_light_dur;                /**< In s */
	unsigned int pedestrian_call_btn_latency;     /**< In s */
	unsigned int overload_extra_light_time;       /**< In s */
	unsigned int overloaded_way_threshold;        /**< In cars */
	unsigned int cars_per_hour[WAY_COUNT];        /**< In car/h */
	unsigned int pedestrians_per_hour[WAY_COUNT]; /**< In pedestrian/h */
	float max_car_velocity;                       /**< In km/h */
	float max_pedestrian_velocity;                /**< In km/h */
	float time_multiplier;                        /**< Time multiplier of the simulation */
} CrossroadConfigBuilder;

/**
 * \enum DeserializeError
 * \brief Enumerates the various errors than can occur during the deserialization process.
 */
typedef enum
{
	NO_ERROR = 0,               /**< No error */
	ERR_FILE_NOT_FOUND = -1,    /**< File has not been found */
	ERR_BAD_FORMAT = -2,        /**< File is not properly formatted */
	ERR_BAD_FIELD_NAME = -3,    /**< Field name does not exist */
	ERR_BAD_FIELD_VALUE = -4    /**< Incorrect field value */
} DeserializeError;

/**
 * \struct DeserializeError
 * \brief Contains details about an error location in the configuration file.
 */
typedef struct
{
	DeserializeError err;       /**< The error */
	unsigned int err_line;      /**< The line at which the error occured */
	unsigned int err_col;       /**< The column at which the error occured */
} DeserializeErrorInfo;

/**
 * \brief Deserializes a CrossroadConfig from a file.
 * \param[out] config The CrossroadConfig to initialize from the file.
 * \param[in] config_file_path The path to the configuration file.
 * \param[out] err_info If an ERR_BAD_FORMAT, ERR_BAD_FIELD_NAME or ERR_BAD_FIELD_VALUE error occurs, 
 *                      gives details about the error location in the configuration file (optional,
 *                      can be set to NULL).
 * \return NO_ERROR on success, another value from the <DeserializeError> enum characterizing the error otherwise.
 */
DeserializeError crconfig_deserialize(CrossroadConfig* config, char const* config_file_path, DeserializeErrorInfo* err_info);

/**
 * \brief Prints a deserialize error to standard output.
 * \param[in] err_info The error info.
 */
void crconfig_print_err(DeserializeErrorInfo const* err_info);

/** To implement if needed: */
/**
 * \brief Serializes a CrossroadConfig to a file.
 * \param[in] config The CrossroadConfig to serialize.
 * \param[in] config_file_path The path to the file where to save the configuration.
 * \return 0 on success, a value different from 0 otherwise.
 */
//int crconfig_serialize(CrossroadConfig const* config, char const* config_file_path);

#endif // CRCONFIG_H_INCLUDED

%{
#include <crconfig.h>
#include <stddef.h>

/**
 * \enum ConfigField
 * \brief Enumerates the various fields of the CrossroadConfigBuilder structure.
 */
typedef enum
{
	STEP_PERIOD = 0,
	GREEN_LIGHT_DUR,
	YELLOW_LIGHT_DUR,
	PEDESTRIAN_CALL_BTN_LATENCY,
	OVERLOAD_EXTRA_LIGHT_TIME,
	OVERLOADED_WAY_THRESHOLD,
	CARS_PER_HOUR,
	PEDESTRIANS_PER_HOUR,
	MAX_CAR_VELOCITY,
	MAX_PEDESTRIAN_VELOCITY,
	TIME_MULTIPLIER
} ConfigField;

/**
 * \enum ConfigFieldType
 * \brief Enumerates the different types of the fields of the CrossroadConfigBuilder structure.
 */
typedef enum
{
	TYPE_UINT,              /**< Unsigned int type */
	TYPE_FLOAT              /**< Float type */
} ConfigFieldType;

/**
 * \struct ConfigFieldInfo
 * \brief Contains information on a field of the CrossroadConfigBuilder.
 */
typedef struct configfieldinfo
{
	char const* name;        /**< Name of the field */
	ConfigFieldType type;    /**< Type of the field */
	size_t offset;           /**< Offset in bytes of the field from the beginning of the CrossroadConfigBuilder structure */
	size_t count;            /**< Number of values in this field */
} ConfigFieldInfo;
%}
%readonly-tables
%switch=1
%struct-type

%define lookup-function-name get_field_info
%define hash-function-name hash_field_name

struct configfieldinfo;
%%
STEP_PERIOD, TYPE_UINT, offsetof(CrossroadConfigBuilder, step_period), 1
GREEN_LIGHT_DUR, TYPE_UINT, offsetof(CrossroadConfigBuilder, green_light_dur), 1
YELLOW_LIGHT_DUR, TYPE_UINT, offsetof(CrossroadConfigBuilder, yellow_light_dur), 1
PEDESTRIAN_CALL_BTN_LATENCY, TYPE_UINT, offsetof(CrossroadConfigBuilder, pedestrian_call_btn_latency), 1
OVERLOAD_EXTRA_LIGHT_TIME, TYPE_UINT, offsetof(CrossroadConfigBuilder, overload_extra_light_time), 1
OVERLOADED_WAY_THRESHOLD, TYPE_UINT, offsetof(CrossroadConfigBuilder, overloaded_way_threshold), 1
MAX_CAR_VELOCITY, TYPE_FLOAT, offsetof(CrossroadConfigBuilder, max_car_velocity), 1
MAX_PEDESTRIAN_VELOCITY, TYPE_FLOAT, offsetof(CrossroadConfigBuilder, max_pedestrian_velocity), 1
TIME_MULTIPLIER, TYPE_FLOAT, offsetof(CrossroadConfigBuilder, time_multiplier), 1
CARS_PER_HOUR, TYPE_UINT, offsetof(CrossroadConfigBuilder, cars_per_hour), WAY_COUNT
PEDESTRIANS_PER_HOUR, TYPE_UINT, offsetof(CrossroadConfigBuilder, pedestrians_per_hour), WAY_COUNT

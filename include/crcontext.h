/**
 * \file
 * \brief 	Context of a crossroad simulator.
 * \date    Created:  18/12/2018
 * \date    Modified: 29/12/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 *
 * All the functions declared in this file must only be called from the simulator thread,
 * unless explicitly stated otherwise in the function documentation.
 */
 
#ifndef CRCONTEXT_H_INCLUDED
#define CRCONTEXT_H_INCLUDED

#include <pthread.h>
#include <way.h>
#include <stdbool.h>
#include <crossroad.h>
#include <crconfig.h>
#include <entity.h>
#include <entity_pool.h>

#define LIGHT_COLOR_COUNT 3    /**< Number of light colors */

/**
 * \enum LightColor
 * \brief Enumerates the different colors of a traffic light.
 */
typedef enum
{
	COLOR_GREEN = 0,      /**< Green color */
	COLOR_YELLOW,         /**< Yellow color */
	COLOR_RED             /**< Red color */
} LightColor;

/**
 * \struct CrossroadContext
 * \brief Context of a crossroad simulator.
 */
typedef struct crcontext
{
	CrossroadConfig const* config;                    /**< Configuration of the simulator */
	LightColor traffic_light_colors[AXIS_COUNT];      /**< Color of the traffic lights */
	Way ways[WAY_COUNT][ENTITY_TYPE_COUNT];           /**< Ways of the crossroad */
	EntityPool entity_pool;                           /**< Pool of entities */
	unsigned int time_before_light_change;            /**< Duration before a light state change (in ms) */
	bool overload_extra_light_time_added;             /**< true if extra time was added to the current green light duration, false otherwise */
	bool is_computing_update;                         /**< true if the simulator is currently computing the next update, false otherwise */
	pthread_cond_t on_update_cond;                    /**< Blocks until a simulator update occur */
	pthread_mutex_t on_update_mutex;                  /**< Mutex protecting the previous condition and the associated <is_computing_update> flag */
	pthread_barrier_t update_step_completed_barrier;  /**< Blocks until the next step in the simulator update process */
} CrossroadContext;

/**
 * \brief Initialises a crossroad context.
 * \param[out] ctx The context to initialise.
 * \param[in] config The simulator configuration.
 * \return 0 on success, a negative number otherwise.
 */
int crcontext_init(CrossroadContext* ctx, CrossroadConfig const* config);

/**
 * \brief Spawns a car in the crossroad.
 * \param[in] ctx The context.
 * \param[in] spawn_way The way at beginning of which the car must spawn.
 * \param[in] dest_way The destination way of the car.
 * \return 0 on success, a negative number otherwise.
 *
 * In the current implementation the destination way is not taken into account.
 */
int crcontext_spawn_car(CrossroadContext* ctx, WayId spawn_way, WayId dest_way);

/**
 * \brief Spawns a pedestrian in the crossroad.
 * \param[in] ctx The context.
 * \param[in] spawn_way The way at beginning of which the pedestrian must spawn.
 * \param[in] dest_way The destination way of the car.
 * \return 0 on success, a negative number otherwise.
 *
 * In the current implementation the destination way is not taken into account.
 */
int crcontext_spawn_pedestrian(CrossroadContext* ctx, WayId spawn_way, WayId dest_way);

/**
 * \brief Computes the next update asynchronously.
 * \param[in] ctx The context.
 */
void crcontext_compute_update_async(CrossroadContext* ctx);

/**
 * \brief Effectively applies the previously computed update.
 * \param[in] ctx The context.
 *
 * The update computation must be started with <crcontext_compute_update_async> before calling this function.
 */
void crcontext_apply_update_sync(CrossroadContext* ctx);

/**
 * \brief Indicates whether or not the specified waiting entity can cross the crossroad.
 * \param[in] ctx The context.
 * \param[in] entity The entity.
 * \return true if the entity can cross the crossroad, false otherwise.
 *
 * This function is only intended to be called from the entity threads.
 */
bool crcontext_can_entity_cross(CrossroadContext* ctx, Entity const* entity);

/**
 * \brief Notifies the context that an entity has started sleeping in the light queue.
 * \param[in] ctx The context.
 * \param[in] entity The entity.
 *
 * This function is only intended to be called from the entity threads.
 */
void crcontext_notify_entity_starts_sleeping_in_light_queue(CrossroadContext* ctx, Entity* entity);

/**
 * \brief Waits until the computation of the next update starts.
 * \param[in] ctx The context.
 *
 * This function is only intended to be called from the entity threads.
 */
void crcontext_wait_compute_update(CrossroadContext* ctx);

/**
 * \brief Waits until the next update step begins, after the update computation has started.
 * \param[in] ctx The context.
 *
 * This function is only intended to be called from the entity threads.
 */
void crcontext_wait_next_update_step(CrossroadContext* ctx);

/**
 * \brief Stops all entity threads and frees all the memory associated with the context.
 * \param[in] ctx The context.
 */
void crcontext_free(CrossroadContext* ctx);

#endif // CRCONTEXT_H_INCLUDED

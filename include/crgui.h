/**
 * \file
 * \brief 	GUI for a crossroad simulator.
 * \date    Created:  23/12/2018
 * \date    Modified: 25/12/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */
 
#ifndef CRGUI_H_INCLUDED
#define CRGUI_H_INCLUDED

#include <crsim.h>
#include <crcontext.h>
#include <entity.h>
#include <SDL2/SDL.h>

/**
 * \struct CrossroadGui
 * \brief The GUI of the crossroad simulator.
 */
typedef struct
{
	CrossroadSimulator* sim;                    /**< The simulator */
	SDL_Window* window;                         /**< The GUI window */
	SDL_Renderer* renderer;                     /**< The GUI renderer */
	SDL_Texture* background;                    /**< The GUI background image */
	SDL_Texture* entities[ENTITY_TYPE_COUNT];   /**< The entity sprites */
	SDL_Texture* lights[LIGHT_COLOR_COUNT];     /**< The traffic light sprites */
} CrossroadGui;

/**
 * \brief Initializes a crossroad GUI.
 * \param[out] gui The crossroad GUI.
 * \param[in] sim The simulator.
 * \return 0 on success, a negative number otherwise.
 */
int crgui_init(CrossroadGui* gui, CrossroadSimulator* sim);

/**
 * \brief Runs the simulator and synchronizes the GUI with the latter.
 * \param[in] gui The crossroad GUI.
 * \return 0 on success, a negative number otherwise.
 *
 * On success, this function returns only when the user choose to close the GUI,
 * stopping the simulator.
 */
int crgui_run(CrossroadGui* gui);

/**
 * \brief Frees all the memory associated with the GUI.
 * \param[in] gui The crossroad GUI.
 */
void crgui_free(CrossroadGui* gui);

#endif // CRGUI_H_INCLUDED

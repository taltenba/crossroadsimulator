/**
 * \file
 * \brief 	Crossed segments of the crossroad (ie. way segments that are crossed by another way).
 * \date    Created:  18/12/2018
 * \date    Modified: 18/12/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#ifndef CROSSED_SEG_H_INCLUDED
#define CROSSED_SEG_H_INCLUDED 

#include <entity.h>

#define CAR_CROSSED_SEG_COUNT 4
#define PEDESTRIAN_CROSSED_SEG_COUNT 2

/**
 * \enum CrossedWay
 * \brief Enumerates the direction from which a way can be crossed.
 */
typedef enum
{
	WAY_LEFT,   /**< From the left */
	WAY_RIGHT   /**< Form the right */
} CrossedWay;

/**
 * \struct CrossedSegment
 * \brief Segment of a way that is crossed by another way.
 *
 * The fields values of the crossed segment are relative to a specific way and from the point of
 * view of a specific entity type on this way.
 *
 * Exemple from the point of view of cars:
 * 		When they cross the crossroad, the first segment that cars cross is a pedestrian way 
 * 		segment (second half of the crosswalk). On this segment cars cross the path of
 *      pedestrians (rival_entity_type = ENTITY_PEDESTRIAN), and the latter arrive from the
 *      left (way = WAY_LEFT). From the point of view of the cars, the crosswalk is located 
 *      immediately at the start of the crossroad (distance_from_crossroad_start = 0.0f) and
 *      the segment length is the width of the crosswalk (ref_end_x = CAR_CROSSROAD_START_X 
 *      + CROSSWALK_WIDTH).
 */
typedef struct
{
	CrossedWay way;                        /**< The way that is crossed, defined by the direction from which the met entities move on the crossed way */
	EntityType met_entity_type;            /**< The type of the met entities that are on the crossed way */
	float distance_from_crossroad_start;   /**< The distance between the start of the segment and the start of the crossroad */
	float end_x;                           /**< The x-value of the end of the segment */
	float end_y;                           /**< The y-value of the end of the segment */
} CrossedSegment;

extern CrossedSegment const CAR_CROSSED_SEGS[CAR_CROSSED_SEG_COUNT];               /**< Segments crossed by the car ways */
											
extern CrossedSegment const PEDESTRIAN_CROSSED_SEGS[PEDESTRIAN_CROSSED_SEG_COUNT]; /**< Segments crossed by the pedestrian ways */

/**
 * \brief Returns the crossed segments for a specified entity way.
 * \param[in] type The way entity type.
 * \param[out] count The number of crossed segments for the specified entity way.
 * \return The crossed segments for the specified entity way.
 */
CrossedSegment const* crossedsegs_get(EntityType type, int* count);
											
#endif // CROSSED_SEG_H_INCLUDED

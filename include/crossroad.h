/**
 * \file
 * \brief 	Constants and utility macros relative to the axis and ways of a crossroad.
 * \date    Created:  16/12/2018
 * \date    Modified: 17/12/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */
 
#ifndef CROSSROAD_H_INCLUDED
#define CROSSROAD_H_INCLUDED

#define MAP_WIDTH 60.0f
#define MAP_HEIGHT 60.0f
#define ROAD_WIDTH 3.0f
#define CROSSWALK_WIDTH 2.5f
#define CROSSWALK_HALF_LENGTH ROAD_WIDTH
#define CROSSWALK_LENGTH (2 * CROSSWALK_HALF_LENGTH)
#define CROSSWALK_OFFSET_LENGTH 0.75f
#define CAR_MIDROAD_DISTANCE 0.5f
#define PEDESTRIAN_ROAD_DISTANCE 0.5f
#define CAR_WAY_CROSSROAD_HALF_LENGTH (CROSSWALK_WIDTH + ROAD_WIDTH)
#define CAR_WAY_CROSSROAD_LENGTH (2 * CAR_WAY_CROSSROAD_HALF_LENGTH)

#define MAP_START_X (-MAP_WIDTH / 2)
#define MAP_END_X (MAP_WIDTH / 2)
#define CROSSWALK_OFFSET -CROSSWALK_OFFSET_LENGTH
#define ROAD_POS_Y -CAR_MIDROAD_DISTANCE
#define SIDEWALK_POS_Y (-ROAD_WIDTH + CROSSWALK_OFFSET - PEDESTRIAN_ROAD_DISTANCE)

#define CAR_WAY_CROSSROAD_START_X (-ROAD_WIDTH - CROSSWALK_WIDTH + 2 * CROSSWALK_OFFSET)
#define CAR_WAY_CROSSROAD_MID_X 0.0f
#define CAR_WAY_CROSSROAD_END_X (-CAR_WAY_CROSSROAD_START_X)
#define CAR_WAY_1ST_CROSSWALK_START_X CAR_WAY_CROSSROAD_START_X
#define CAR_WAY_1ST_CROSSWALK_END_X (-ROAD_WIDTH + CROSSWALK_OFFSET)
#define CAR_WAY_2ND_CROSSWALK_START_X ROAD_WIDTH - CROSSWALK_OFFSET
#define CAR_WAY_2ND_CROSSWALK_END_X (CAR_WAY_2ND_CROSSWALK_START_X + CROSSWALK_WIDTH)

#define PEDESTRIAN_WAY_CROSSROAD_START_X (-ROAD_WIDTH)
#define PEDESTRIAN_WAY_CROSSROAD_MID_X 0.0f
#define PEDESTRIAN_WAY_CROSSROAD_END_X ROAD_WIDTH

#define AXIS_WAY_1(axis) ((WayId) (axis))                        /**< First way of an axis */
#define AXIS_WAY_2(axis) ((WayId) ((axis) + 2))                  /**< Second way of an axis */
#define NEXT_AXIS_WAY(way) ((WayId) ((way) + 2))                 /**< Next way on the axis of the specified way */
#define OTHER_AXIS(axis) ((Axis) ((axis) ^ 1))                   /**< The other crossroad axis */
#define WAY_AXIS(way) ((Axis) ((way) % AXIS_COUNT))              /**< The axis of the specified way */
#define OPPOSITE_WAY(way) ((WayId) (((way) + 2) % WAY_COUNT))    /**< The opposite way on the axis of the specified way */
#define LEFT_WAY(way) ((WayId) (((way) - 1) % WAY_COUNT))        /**< The perpendicular way crossing the specified way from the left */
#define RIGHT_WAY(way) ((WayId) (((way) + 1) % WAY_COUNT))       /**< The perpendicular way crossing the specified way from the right */

#define AXIS_COUNT 2    /**< Number of axis of the crossroad */
#define WAY_COUNT 4     /**< Number of ways of the crossroad */

/**
 * \enum Axis
 * \brief Enumerates the two axis of the crossroad.
 */
typedef enum
{
	AXIS_HORIZONTAL = 0,   /**< Horizontal axis */
	AXIS_VERTICAL          /**< Vertical axis */
} Axis;

/**
 * \enum WayId
 * \brief Enumerates the four different ways of the crossroad.
 */
typedef enum
{
	WAY_WE = 0,   /**< West->East way */
	WAY_SN,       /**< South->North way */
	WAY_EW,       /**< East->West way */
	WAY_NS        /**< North->South way */
} WayId;

#endif // CROSSROAD_H_INCLUDED

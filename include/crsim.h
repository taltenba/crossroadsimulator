/**
 * \file
 * \brief 	Simulator of a crossroad (4-way intersection) with traffic lights and crosswalks.
 * \date    Created:  10/12/2018
 * \date    Modified: 20/12/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 *
 * All the function declared in this file must be called from one single thread.
 */

#ifndef CRSIMULATOR_H_INCLUDED
#define CRSIMULATOR_H_INCLUDED

#include <pthread.h>
#include <semaphore.h>
#include <crconfig.h>
#include <crcontext.h>

/**
 * \struct CrossroadSimulator
 * \brief A crossroad simulator.
 *
 * The simulator is *not* thread safe and must therefore be local to a single thread.
 * The simulator internally use the C standard random number generator and the latter must
 * therefore be properly initialized.
 */
typedef struct
{
	CrossroadContext ctx;              /**< Current context of the simulated crossroad */
	CrossroadConfig config;            /**< Configuration of the simulator */
	pthread_t sim_thread;              /**< Simulation thread */
	bool is_running;                   /**< Indicates if the simulation is running */
	sem_t ctx_updated_sem;             /**< Semaphore used to notify the simulation observer that the crossroad context has been updated */
	sem_t ctx_update_proccessed;       /**< Semaphore used to notify the simulator that the crossroad context update has been processed by the simulation observer */
} CrossroadSimulator;

/**
 * \brief Creates a new crossroad simulator.
 * \param[in] config_file_path The path of the simulator configuration file.
 * \param[out] err_info On deserialization error, contains information on the error.
 * \return A newly initialized crossroad simulator on success, NULL otherwise.
 *
 * On error, either the err_info indicates NO_ERROR, in which case the crossroad simulator
 * memory allocation failed, or the err_info indicates an error, in which case the failure
 * come from a deserialization error.
 */
CrossroadSimulator* crsim_new(char const* config_file_path, DeserializeErrorInfo* err_info);

/**
 * \brief Starts the simulation.
 * \param[in] sim The simulator.
 * \return 0 on success, a non-zero error number otherwise.
 *
 * If the simulation is already running, this function has no effect and return 0.
 */ 
int crsim_start(CrossroadSimulator* sim);

/**
 * \brief Stops the simulation.
 * \param[in] sim The simulator.
 *
 * If the simulation is running, the simulation thread is synchronously stopped
 * and the simulation context is reset.
 * If the simulation is not running this function has no effect.
 */
void crsim_stop(CrossroadSimulator* sim);

/**
 * \brief Waits with a timeout for the next simulator update.
 * \param[in] sim The simulator.
 * \param[in] ms The timeout.
 * \return 0 if a simulator update occurred, a negative number if the timeout
 *         expired before the update has occured.
 */
int crsim_timedwait_update(CrossroadSimulator* sim, unsigned int ms);

/**
 * \brief Notifies the simulator that its last update has been processed by the simulator observer.
 * \param[in] sim The simulator.
 *
 * After an update, the simulator waits the observer to have finished the update processing before
 * issuing the next update.
 */
void crsim_notify_update_processed(CrossroadSimulator* sim);

/**
 * \brief Frees all the memory associated with a simulator.
 * \param[in] sim The simulator.
 *
 * If the simulation is running, the simulation is first and foremost stopped.
 */
void crsim_destroy(CrossroadSimulator* sim);

#endif // CRSIMULATOR_H_INCLUDED


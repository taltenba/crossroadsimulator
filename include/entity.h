/**
 * \file
 * \brief 	Entity of a crossroad environnement (car or pedestrian).
 * \date    Created:  18/12/2018
 * \date    Modified: 27/12/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */
 
#ifndef ENTITY_H_INCLUDED
#define ENTITY_H_INCLUDED

#include <stdbool.h>
#include <transform.h>
#include <crossroad.h>
#include <semaphore.h>

#define ENTITY_TYPE_COUNT 2        /**< Number of entity types */

/**
 * \enum EntityType
 * \brief Enumerates the different types of entity.
 */
typedef enum
{
	ENTITY_CAR = 0,
	ENTITY_PEDESTRIAN
} EntityType;

/**
 * \enum EntityState
 * \brief Enumerates the different states in which an entity can be.
 */
typedef enum
{
	STATE_DISABLED,                /**< Entity disabled */
	STATE_AWAKE,                   /**< Entity enabled and not sleeping */
	STATE_FALLING_ASLEEP,          /**< Entity enabled and will sleep next step if cannot move */
	STATE_SLEEPING                 /**< Entity enabled and sleeping */
} EntityState;

struct crcontext;

/**
 * \struct Entity
 * \brief A crossroad environnement entity.
 */
typedef struct entity
{
	struct crcontext* ctx;          /**< The crossroad context */
	EntityType type;                /**< The entity type */
	EntityState state;              /**< The entity state */
	Transform transform;            /**< The entity transform (position + rotation) */
	WayId cur_way;                  /**< The current way in which the entity is moving */
	WayId dest_way;                 /**< The way towards which the entity is going */
	float velocity;                 /**< The current velocity of the entity */
	bool is_in_light_queue;         /**< true iff the entity is in the traffic light queue */
	struct entity* prec_entity;     /**< The preceding entity in the current way */
	struct entity* succ_entity;     /**< The succeding entity in the current way */
	sem_t can_move_sem;             /**< Sempahore used to get the entity to sleep until it can move again */ 
	pthread_t thread;               /**< The thread of the entity */
} Entity;

/**
 * \brief Intializes an entity.
 * \param[out] entity The entity.
 * \param[in] type The entity type.
 * \param[in] ctx The crossroad context.
 * \param[in] transform The initial entity transform.
 * \param[in] src_way The initial entity way.
 * \param[in] dest_way The entity destination way.
 *
 * The entity is initially disabled.
 */
void entity_init(Entity* entity, EntityType type, struct crcontext* ctx, Transform const* transform, WayId src_way, WayId dest_way);

/**
 * \brief Enables a currently disabled entity.
 * \param[in] entity The disabled entity.
 */
void entity_enable(Entity* entity);

/**
 * \brief Disables a currently enabled entity.
 * \param[in] entity The enabled entity.
 */
void entity_disable(Entity* entity);

/**
 * \brief Wakes up a currently sleeping entity.
 * \param[in] entity The sleeping entity.
 */
void entity_wake_up(Entity* entity);

/**
 * \brief Returns the maximum velocity of an entity.
 * \param[in] entity The entity.
 * \return The maximum velocity of the specified entity.
 */
float entity_get_max_velocity(Entity const* entity);

/**
 * \brief Returns the length of an entity.
 * \param[in] entity The entity.
 * \return The length of the specified entity in meters.
 */
float entity_get_length(Entity const* entity);

/**
 * \brief Returns the safe distance that an entity must observe with the other entities.
 * \param[in] entity The entity.
 * \return The the safe distance that the specified entity must observe with the other entities.
 */
float entity_get_safe_distance(Entity const* entity);

/**
 * \brief Returns the current local space x-position of the top-left corner of an entity.
 * \param[in] entity The entity.
 * \return The current local space x-position of the top-left corner of the specified entity.
 */
float entity_get_start_x(Entity const* entity);

/**
 * \brief Returns the current local space x-position of the bottom-left corner of an entity.
 * \param[in] entity The entity.
 * \return The current local space x-position of the bottom-left corner of the specified entity.
 */
float entity_get_end_x(Entity const* entity);

/**
 * \brief Returns the closest local space x-position where an entity following the specified one
 *        can go in order to respect safe distance with the specified entity.
 * \param[in] entity The entity.
 * \return The closest local space x-position where an entity following the specified one
 *        can go in order to respect safe distance with the specified entity.
 */
float entity_get_safe_end_x(Entity const* entity);

/**
 * \brief Indicates whether or not an entity has moved during the last simulation update.
 * \param[in] entity The entity.
 * \return true if the entity has moved during the last update, false otherwise.
 */
bool entity_has_moved(Entity const* entity);

/**
 * \brief Indicates whether or not the front of an entity has passed the start of the crossroad.
 * \param[in] entity The entity.
 * \return true if the front of the specified entity has passed the start of the crossroad, false
 *         otherwise.
 */
bool entity_has_passed_crossroad_start(Entity const* entity);

/**
 * \brief Indicates whether or not the back of an entity has passed the end of the crossroad.
 * \param[in] entity The entity.
 * \return true if the back of the specified entity has passed the end of the crossroad, false
 *         otherwise.
 */
bool entity_has_passed_crossroad_end(Entity const* entity);

/**
 * \brief Stops the entity thread if running and frees all the memory associated with an entity.
 * \param[in] entity The entity.
 */
void entity_free(Entity* entity);

#endif // ENTITY_H_INCLUDED

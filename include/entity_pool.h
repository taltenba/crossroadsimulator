/**
 * \file
 * \brief 	Pool of entities.
 * \date    Created:  18/12/2018
 * \date    Modified: 18/12/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#ifndef ENTITYPOOL_H_INCLUDED
#define ENTITYPOOL_H_INCLUDED

#include <entity.h>
#include <fsmem_pool.h>

/**
 * \struct EntityPool
 * \brief Pool of entities.
 */
typedef FSMemPool EntityPool;

/**
 * \brief Intializes an entity pool.
 * \param[in] pool The entity pool.
 */
void entitypool_init(EntityPool* pool);

/**
 * \brief Gets and initializes an entity from the pool.
 * \param[in] pool The entity pool.
 * \param[in] type The entity type.
 * \param[in] ctx The crossroad context.
 * \param[in] transform The entity initial transform.
 * \param[in] src_way The entity initial way.
 * \param[in] dest_way The entity destination way.
 * \return An intialized entity on success, NULL otherwise (out-of-memory).
 */
Entity* entitypool_get(EntityPool* pool, EntityType type, struct crcontext* ctx, Transform const* transform, WayId src_way, WayId dest_way);

/**
 * \brief Frees an entity from the pool.
 * \param[in] pool The entity pool.
 * \param[in] entity The entity.
 */
void entitypool_free(EntityPool* pool, Entity* entity);

/**
 * \brief Frees all the memory associated with memory pool.
 * \param[in] pool The entity pool.
 */
void entitypool_clear(EntityPool* pool);

#endif // ENTITYPOOL_H_INCLUDED

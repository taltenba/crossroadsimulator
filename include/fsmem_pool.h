/**
 * \file
 * \brief 	Fixed-size memory pool
 * \date 	Created: 09/05/2017
 * \date	Modified: 18/11/2017
 * \author 	ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 *
 * This memory pool enables dynamic allocation of fixed-size memory blocks.
 *
 * These blocks are stored in bigger chunks, allocated with only once malloc call.
 *
 * The use of such a memory pool reduces then the memory overheads due to the allocation of a great deal of 
 * small memory blocks using malloc. Minimizing malloc calls, the memory pool also highly increase the time 
 * efficiency.
 *
 * Last but not least, blocks of the same chunk are stored contiguously which enhances spatial locality 
 * of memory and is therefore more cache-friendly, in particular if few or no blocks are freed before the 
 * memory of the whole pool is released.
 */

#ifndef FSMEM_POOL_H
#define FSMEM_POOL_H

#include <stdlib.h>

/**
 * \union FSMemBlock
 * \brief Elementary memory block, storing user data
 */
typedef union fsblock FSMemBlock;

/**
 * \struct FSMemChunk
 * \brief Memory chunk composed of multiple memory blocks
 */
typedef struct fschunk FSMemChunk;

/**
 * \struct FSMemPool
 * \brief Fixed-size memory pool
 */
typedef struct
{
    FSMemChunk* cur_chunk;		/**< Current memory chunk */
    FSMemBlock* next_block;		/**< Next free block in the current chunk */
    FSMemBlock* last_block;		/**< End of the current chunk */
    FSMemBlock* free_blocks;	/**< Linked-list of free blocks */
    size_t chunk_size;			/**< Size of a memory chunk (in bytes) */
    size_t block_size;			/**< Size of a memory block (in bytes) */
} FSMemPool;

/**
 * \brief Initializes a new fixed-size memory pool
 * \param[out] pool Memory pool to intialize
 * \param[in] block_size Size of a data block (in bytes)
 * \param[in] chunk_capacity Number of data blocks per chunk
 * \param[in] init_capacity Number of data blocks to allocate at the initialization
 */
void fsmpool_init(FSMemPool* pool, size_t block_size, size_t chunk_capacity, size_t init_capacity);

/**
 * \brief Allocates a new memory block from the memory pool
 * \param[in] pool The memory pool
 * \return The newly allocated block on success, NULL otherwise (out of memory)
 */
void* fsmpool_alloc(FSMemPool* pool);

/**
 * \brief Frees a formely allocated memory block of a memory pool
 * \param[in] pool The memory pool
 * \param[in] block The memory block to free
 */
void fsmpool_free(FSMemPool* pool, void* block);

/**
 * \brief Frees all the memory associated with a memory pool
 * \param[in] pool The memory pool
 */
void fsmpool_clear(FSMemPool* pool);

#endif /* FSMEM_POOL_H */

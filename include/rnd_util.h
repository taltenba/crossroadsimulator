/**
 * \file
 * \brief   Utility routines related to pseudo-random number generation.
 * \date    Created:  20/05/2017
 * \date    Modified: 20/05/2017
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */
 
#ifndef RND_UTIL_H_INCLUDED
#define RND_UTIL_H_INCLUDED

#include <stdlib.h>

/**
 * \brief Generates a pseudo-random float in the range [0.0f, 1.0f].
 * \return A pseudo-random float in the range [0.0f, 1.0f].
 *
 * Note: this function use the C standard PRNG and is *not* thread safe.
 */
inline float randf()
{
	return (float) rand() / RAND_MAX;
}

#endif // RND_UTIL_H_INCLUDED

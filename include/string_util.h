/**
 * \file
 * \brief 	Utility routines related to strings
 * \date    Created:  11/02/2017
 * \date    Modified: 16/12/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#ifndef STRING_UTIL_H_INCLUDED
#define STRING_UTIL_H_INCLUDED

#include <stdbool.h>
#include <time.h>

/**
 * \brief Indicates if the specified character is a blank character.
 * \param[in] c The character.
 * \return true if the character is blank, false otherwise.
 *
 * A character is considered blank if and only if it is equal to ' ' or '\t'.
 */
inline bool is_blank(char c)
{
	return c == ' ' || c == '\t';
}

/**
 * \brief Hashes a string.
 * \param[in] str The string to hash.
 * \return The hash of the string.
 */
unsigned long hash_string(char const* str);

/**
 * \brief Finds the first non-whitespace character in a string
 * \param[in] str The string
 * \return The index of the first non-whitespace char. If the string contains only whitespaces, 
 *         the index of the first '\0' is returned.
 *
 * Are regarded as whitespace chars : ' ' (0x20), '\t' (0x09), '\n' (0x0a), '\v' (0x0b), 
 *                                    '\f' (0x0c), and '\r' (0x0d)
 */
size_t find_nonspace(char const* str);

/**
 * \brief Indicates if a string contains at least one character that is not a whitespace
 * \param[in] str The string
 * \param[out] begin The index of the first non-whitespace char found in the string. 
 *                   If the string contains only whitespaces, the index of the first '\0' is returned.
 *                   Can be NULL if not required.
 *
 * All the characters regarded as whitespaces are defined in the <find_nonspace> function 
 * documentation.
 */
bool contains_only_spaces(char const* str, size_t* begin);

/**
 * \brief Converts a string to a boolean
 * \param[in] str The string
 * \param[out] result The boolean equivalent of the string on success
 * \return 0 on success, a non-zero value otherwise
 *
 * The operation succeed iff :
 *     - The string is not empty
 *     - Contains only (in addition to the '\0') the caracter '0' (for FALSE) or '1' (for TRUE)
 * 
 * The potential whitespaces at the beginning and the end of the string are ignored.
 * On failure, nothing is written into result.
 */
int parse_bool(char const* str, bool* result);

/**
 * \brief Converts a string to a char integer
 * \param[in] str The string
 * \param[out] result The integer equivalent of the string on success
 * \return 0 on success, a non-zero value otherwise
 *
 * The operation succeed iff :
 *     - The string is not empty.
 *     - Contains only (in addition to the '\0') an integer number with an optional sign character
 *       ('+' or '-') at the beginning.
 *     - The integer number do not produce overflow.
 * 
 * The potential whitespaces at the beginning and the end of the string are ignored.
 * On failure, nothing is written into result.
 */
int parse_char(char const* str, char* result);

/**
 * \brief Converts a string to an int
 * \param[in] str The string
 * \param[out] result The integer equivalent of the string on success
 * \return 0 on success, a non-zero value otherwise
 *
 * The operation succeed iff :
 *     - The string is not empty.
 *     - Contains only (in addition to the '\0') an integer number with an optional sign character
 *       ('+' or '-') at the beginning.
 *     - The integer number do not produce overflow.
 * 
 * The potential whitespaces at the beginning and the end of the string are ignored.
 * On failure, nothing is written into result.
 */
int parse_int(char const* str, int* result);

/**
 * \brief Converts a string to an long
 * \param[in] str The string
 * \param[out] result The integer equivalent of the string on success
 * \return 0 on success, a non-zero value otherwise
 *
 * The operation succeed iff :
 *     - The string is not empty.
 *     - Contains only (in addition to the '\0') an integer number with an optional sign character
 *       ('+' or '-') at the beginning.
 *     - The integer number do not produce overflow.
 * 
 * The potential whitespaces at the beginning and the end of the string are ignored.
 * On failure, nothing is written into result.
 */
int parse_long(char const* str, long* result);

/**
 * \brief Converts a hexadecimal string to a unsigned char
 * \param[in] str The string
 * \param[out] result The integer equivalent of the string on success
 * \return 0 on success, a non-zero value otherwise
 *
 * The operation succeed iff :
 *     - The string is not empty.
 *     - Contains only (in addition to the '\0') a postive hexadecimal integer number with an 
 *       optional sign '+' character at the beginning.
 *     - The integer number do not produce overflow.
 * 
 * The potential whitespaces at the beginning and the end of the string are ignored.
 * On failure, nothing is written into result.
 */
int parse_uchar_hex(char const* str, unsigned char* result);

/**
 * \brief Converts a string to an unsigned int
 * \param[in] str The string
 * \param[out] result The integer equivalent of the string on success
 * \return 0 on success, a non-zero value otherwise
 *
 * The operation succeed iff :
 *     - The string is not empty.
 *     - Contains only (in addition to the '\0') a postive integer number with an optional sign 
 *       '+' character at the beginning.
 *     - The integer number do not produce overflow.
 * 
 * The potential whitespaces at the beginning and the end of the string are ignored.
 * On failure, nothing is written into result.
 */
int parse_uint(char const* str, unsigned int* result);

/**
 * \brief Converts a string to an unsigned long
 * \param[in] str The string
 * \param[out] result The integer equivalent of the string on success
 * \return 0 on success, a non-zero value otherwise
 *
 * The operation succeed iff :
 *     - The string is not empty.
 *     - Contains only (in addition to the '\0') a postive integer number with an optional sign 
 *       '+' character at the beginning.
 *     - The integer number do not produce overflow.
 * 
 * The potential whitespaces at the beginning and the end of the string are ignored.
 * On failure, nothing is written into result.
 */
int parse_ulong(char const* str, unsigned long* result);

/**
 * \brief Converts a string to a float
 * \param[in] str The string
 * \param[out] result The number equivalent of the string on success
 * \return 0 on success, a non-zero value otherwise
 *
 * The operation succeed iff :
 *     - The string is not empty.
 *     - Contains only (in addition to the '\0') a number with an optional sign character
 *       ('+' or '-') at the beginning and an optional decimal-point '.' character.
 *     - The integer number do not produce overflow.
 *
 * Detection of underflow is not guaranteed to produce failure, but if it does not the number 0 
 * is written in result.
 * 
 * The potential whitespaces at the beginning and the end of the string are ignored.
 * On failure, nothing is written into result.
 */
int parse_float(char const* str, float* result);

/**
 * \brief Converts a string to a double
 * \param[in] str The string
 * \param[out] result The number equivalent of the string on success
 * \return 0 on success, a non-zero value otherwise
 *
 * The operation succeed iff :
 *     - The string is not empty.
 *     - Contains only (in addition to the '\0') a number with an optional sign character
 *       ('+' or '-') at the beginning and an optional decimal-point '.' character.
 *     - The integer number do not produce overflow.
 *
 * Detection of underflow is not guaranteed to produce failure, but if it does not the number 0 
 * is written in result.
 * 
 * The potential whitespaces at the beginning and the end of the string are ignored.
 * On failure, nothing is written into result.
 */
int parse_double(char const* str, double* result);

/**
 * \brief Converts a string to a date time
 * \param[in] str The string
 * \param[out] result The date time equivalent of the string on success
 * \return 0 on success, a non-zero value otherwise
 *
 * This function accepts only the formats : YYYY-MM-DD HH:MM:SS or YYYY-MM-DD
 * 
 * The potential whitespaces at the beginning and the end of the string are ignored.
 * On failure, nothing is written into result.
 */
int parse_date_time(char const* str, time_t* result);

/**
 * \brief Converts a date time to a string
 * \param[in] date_time The date time
 * \param[out] str The string in the format YYYY-MM-DD HH:MM:SS
 */
void print_date_time(time_t date_time, char* str);

#endif //STRING_UTIL_H_INCLUDED


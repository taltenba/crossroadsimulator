/**
 * \file
 * \brief 	Routines and structures to deal with the position and rotation of objects in a 2D space.
 * \date    Created:  19/12/2018
 * \date    Modified: 19/12/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#ifndef TRANSFORM_H_INCLUDED
#define TRANSFORM_H_INCLUDED

/**
 * \brief Transform located at local_x and local_y in the local space and rotated by 0°.
 */
#define TRANSFORM_0(local_x, local_y) {.local_pos = {local_x, local_y}, .local_to_world_matrix = {{1.0f, 0.0f}, {0.0f, 1.0f}}}

/**
 * \brief Transform located at local_x and local_y in the local space and rotated by 90°.
 */
#define TRANSFORM_90(local_x, local_y) {.local_pos = {local_x, local_y}, .local_to_world_matrix = {{0.0f, -1.0f}, {1.0f, 0.0f}}}

/**
 * \brief Transform located at local_x and local_y in the local space and rotated by 180°.
 */
#define TRANSFORM_180(local_x, local_y) {.local_pos = {local_x, local_y}, .local_to_world_matrix = {{-1.0f, 0.0f}, {0.0f, -1.0f}}}

/**
 * \brief Transform located at local_x and local_y in the local space and rotated by 270°.
 */
#define TRANSFORM_270(local_x, local_y) {.local_pos = {local_x, local_y}, .local_to_world_matrix = {{0.0f, 1.0f}, {-1.0f, 0.0f}}}

/**
 * \struct Position
 * \brief Stores a position in 2D space.
 */
typedef struct
{
	float x;
	float y;
} Position;

/**
 * \struct Transform
 * \brief Stores position and rotation of an object.
 *
 * The transform local position is relative to the 2D coordinate system resulting from
 * the rotation of the world coordinate system by the rotation value of the tranform.
 */
typedef struct
{
	Position local_pos;                  /**< Local coordinates of the transform */ 
	//float rot;                         /**< Rotation in radians of the transform in the world coordinate system */
	float local_to_world_matrix[2][2];   /**< Matrix transforming a point from the local space to the world space */
} Transform;

/**
 * \brief Initializes a tranform.
 * \param[out] transform The transform.
 * \param[in] local_pos_x The local x-coordinate of the transform.
 * \param[in] local_pos_y The local y-coordinate of the transform.
 * \param[in] rot The rotation in radians of the transform in the world space around the world 
 *                coordinate system origin.
 */
void transform_init(Transform* transform, float local_pos_x, float local_pos_y, float rot);

/**
 * \brief Sets the world rotation of the transform around the world coordinate system origin.
 * \param[in] transform The transform.
 * \param[in] rot The rotation in radians in the world space around the world coordinate system origin.
 */
void transform_set_rot(Transform* transform, float rot);

/**
 * \brief Translates a transform in the local space.
 * \param[in] transform The transform.
 * \param[in] delta_x The value of the translation along the local x-axis.
 * \param[in] delta_y The value of the translation along the local y-axis.
 */
void transform_translate(Transform* transform, float delta_x, float delta_y);

/**
 * \brief Gets the world space position of the transform.
 * \param[in] transform The transform.
 * \param[out] world_pos The world position of the transform.
 */
void transform_get_world_pos(Transform const* transform, Position* world_pos);

#endif // TRANSFORM_H_INCLUDED

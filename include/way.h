/**
 * \file
 * \brief 	Way of a crossroad.
 * \date    Created:  18/12/2018
 * \date    Modified: 29/12/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 *
 * All the function declared in this file are not thread safe, unless explicitely
 * stated otherwise in the function documentation.
 */
 
#ifndef WAY_H_INCLUDED
#define WAY_H_INCLUDED

#include <stdlib.h>
#include <stdbool.h>
#include <entity.h>
#include <pthread.h>

/**
 * \struct Way
 * \brief Way of a crossroad.
 *
 * A way contains only entities of the same type.
 */
typedef struct
{
	Entity* first_entity;                /**< First entity of the way */
	Entity* last_entity;                 /**< Last entity of the way */
	Entity* next_entity_to_cross;        /**< Next entity of the way that will cross the crossroad */
	Entity* last_waken_up_queue_entity;  /**< Last entity of the way to have been waken up after sleeping in the light queue */
	Entity* first_crossing_entity;       /**< First entity of the way that is currently crossing the crossroad */
	Entity* last_crossing_entity;        /**< Last entity of the way that is currently crossing the crossroad */
	size_t light_queue_size;             /**< Number of entities that arrived at the traffic light queue and are waiting to be able to cross the crossroad */
	size_t active_entity_count;          /**< Number of entities that are not sleeping */
	pthread_mutex_t mutex;               /**< Mutex used to lock the way */
} Way;

/**
 * \brief Initializes a way.
 * \param[out] way The way.
 */
void way_init(Way* way);

/**
 * \brief Lock the way mutex.
 * \param[in] way The way.
 *
 * This function is thread safe.
 */
void way_lock(Way* way);

/**
 * \brief Unlock the way mutex.
 * \param[in] way The way.
 *
 * This function is thread safe.
 */
void way_unlock(Way* way);

/**
 * \brief Notifies a way that a new entity has spawned at the beginning of the way.
 * \param[in] way The way.
 * \param[in] entity The entity that spawned.
 */
void way_notify_entity_spawned(Way* way, Entity* entity);

/**
 * \brief Notifies a way that the last way entity has exited the map.
 * \param[in] way The way.
 */
void way_notify_entity_exited(Way* way);

/**
 * \brief Notifies a way that an entity of the way has started sleeping in the light queue.
 * \param[in] way The way.
 * \param[in] entity The entity.
 */
void way_notify_entity_starts_sleeping_in_light_queue(Way* way, Entity* entity);

/**
 * \brief Notifies a way that the waiting entities of the way can now cross the crossroad.
 * \param[in] way The way.
 */
void way_notify_entities_can_cross(Way* way);

/**
 * \brief Notifies a way that the first entity of the way that is currently crossing has finished
 *        to cross the crossroad.
 * \param[in] way The way.
 */
void way_notify_entity_crossed(Way* way);

/**
 * \brief Updates the reference to the next entity of the way that will cross the crossroad.
 * \param[in] way The way.
 */
void way_update_next_entity_to_cross(Way* way);

/**
 * \brief Indicates whether or not the traffic light queue of the way is currently empty.
 * \param[in] way The way.
 * \return true if the traffic light queue is currently empty, false otherwise.
 */
bool way_is_light_queue_empty(Way const* way);

/**
 * \brief Indicates whether the specified segment crossed by the way will be free when an entity
 *        arriving from a perpendicular way at the specified velocity and currently at the specified 
 *        distance of the crossed segment, will arrive to the crossed segment.
 * \param[in] way The way.
 * \param[in] seg_end_x The x-coordinate of the end of the crossed segment on the way.
 * \param[in] seg_distance The distance between the entity arriving from a perpendicular way and
 *                         the beginning of the crossed segment.
 * \param[in] velocity The velocity of the entity arriving from a perpendicular way.
 *
 * This function assumes that no other entity of this way will start crossing before the segment is 
 * reached by the entity arriving from a perpendicular way (typically the case during a red or yellow 
 * light).
 */
bool way_crossed_seg_free_when_reached(Way const* way, float seg_end_x, float seg_distance, float velocity);

#endif // WAY_H_INCLUDED

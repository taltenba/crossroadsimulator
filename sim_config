%%%%%%%%%%%%%%%%% Fichier de configuration du simulateur de carrefour à feux %%%%%%%%%%%%%%%%%
% Des commentaires peuvent être écrits en les faisant précéder par le charactère '%'.
% Les nombres réels doivent être écrits en utilisant le '.' comme séparateur décimal.

%%
% Pas de simulation en millisecondes, c'est à dire la durée, en temps simulé, 
% entre deux mises à jour.
% Plus la valeur est petite, plus la simulation est fluide et précise.
%
% Type : Entier positif non-nul
%%
STEP_PERIOD = 80

%%
% Valeur du multiplicateur temporel. 
% Permet d'accélér ou de ralentir la simulation.
% Modifier cette valeur ne change pas la précision de la simulation,
% seule la vitesse de simulation est impactée.
% À noter qu'il peut être nécessaire en pratique d'augmenter la valeur du pas de
% simulation pour que l'effet se fasse sentir. Certains systèmes ne permettent en 
% effet pas d'effectuer de très courtes attentes entre deux pas de simulation.
%
% Type : Réel positif non-nul
%%
TIME_MULTIPLIER = 4

%%
% Durée en secondes du feu vert en régime normal.
% Cette durée sera en pratique arrondie au multiple de pas de simulation supérieur.
%
% Type : Entier positif non-nul
%%
GREEN_LIGHT_DUR = 15

%%
% Durée en secondes du feu orange.
% Cette durée sera en pratique arrondie au multiple de pas de simulation supérieur.
%
% Type : Entier positif non-nul
%%
YELLOW_LIGHT_DUR = 1

%%
% Durée en secondes entre l'appui d'un piéton sur un bouton piéton et sa prise en compte.
% Cette durée sera en pratique arrondie au multiple de pas de simulation supérieur.
%
% Type : Entier positif
%%
PEDESTRIAN_CALL_BTN_LATENCY = 5 

%%
% Durée en secondes qui sera ajoutée en régime dégradé à la durée du feu vert en régime normal.
% Cette durée sera en pratique arrondie au multiple de pas de simulation supérieur.
%
% Type : Entier positif
%%
OVERLOAD_EXTRA_LIGHT_TIME = 5

%%
% Nombre de voitures en attente de traverser à partir duquel une voie de circulation est considérée
% comme surchargée.
%
% Type : Entier positif
%%
OVERLOADED_WAY_THRESHOLD = 5

%%
% Vitesse maximale d'une voiture en km/h.
%
% Type : Réel positif non-nul
%%
MAX_CAR_VELOCITY = 30.0

%%
% Vitesse maximale d'un piéton en km/h.
%
% Type : Réel positif non-nul
%%
MAX_PEDESTRIAN_VELOCITY = 6.0

%%
% Nombre de voitures en moyenne circulant sur les différentes voies de circulation.
%
% Type : Liste d'entiers positifs séparés par des virgules et listés dans l'ordre suivant :
%        voie Ouest-Est, Sud-Nord, Est-Ouest puis Nord-Sud.
%%
CARS_PER_HOUR = 3000, 450, 200, 300

%%
% Nombre de piétons en moyenne circulant sur les différentes voies de circulation.
%
% Type : Liste d'entiers positifs séparés par des virgules et listés dans l'ordre suivant :
%        voie Ouest-Est, Sud-Nord, Est-Ouest puis Nord-Sud.
%%
PEDESTRIANS_PER_HOUR = 50, 100, 300, 150





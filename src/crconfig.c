/**
 * \file
 * \brief 	Configuration of a crossroad simulator.
 * \date    Created:  11/12/2018
 * \date    Modified: 18/12/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include <string_util.h>
#include <crconfig_fields.h>
#include <crconfig.h>

#define IO_BUFFER_SIZE 128	        /**< Size in bytes of the buffer used for IO operations */

#define ARRAY_VALUE_DELIMITER ','   /**< Delimiter used to separate elements of an array in the config file */
#define COMMENT_CHAR '%'            /**< Character used to start a comment in the config file */

/**
 * \brief Parses a configuration builder field.
 * \param[in] builder The builder.
 * \param[in] field_name The name of the field.
 * \param[in] name_len The length of the field name.
 * \param[in] file The configuration file from which the field value should be parsed.
 * \return NO_ERROR on success, another value from the <DeserializeError> enum characterizing the 
 *         error otherwise.
 */
static DeserializeError parse_field(CrossroadConfigBuilder* builder, char const* field_name, size_t name_len, FILE* file)
{
	ConfigFieldInfo const* field_info = get_field_info(field_name, strlen(field_name));
	
	if (field_info == NULL)
		return ERR_BAD_FIELD_NAME;
	
	void* field = (void*) ((char*) builder + field_info->offset);
	
	int c;
	char buffer[IO_BUFFER_SIZE];
	size_t i = 0;
	size_t count = field_info->count;
	int parse_result;
	
	do {
		char* cur_buf = buffer;
		
		while ((c = getc(file)) != EOF && c != '\n' && c != ARRAY_VALUE_DELIMITER && c != COMMENT_CHAR)
			*(cur_buf++) = c;
		
		*cur_buf = '\0';
		
		switch (field_info->type)
		{
			case TYPE_UINT:
				parse_result = parse_uint(buffer, (unsigned int*) field + i);
				break;
			case TYPE_FLOAT:
				parse_result = parse_float(buffer, (float*) field + i);
				break;
		}
		
		++i;
	} while (i < count && parse_result == 0 && c != COMMENT_CHAR);
	
	// Field parsing is successful if all values has been successfully parsed and we reached the end of the line (or the beginning of a comment)
	if (parse_result != 0 || i != count || (c != COMMENT_CHAR && c != EOF && c != '\n'))
		return ERR_BAD_FIELD_VALUE;
	
	if (c == COMMENT_CHAR) // Skip comment
	{
		do
		{
			c = getc(file);
		} while (c != EOF && c != '\n');
	}
	
	return 0;
}

/**
 * \brief Closes the file and optionnally initializes a DeserializeErrorInfo with the given 
 *        location of the error.
 * \param[in] file The file to close.
 * \param[out] err_info The DeserializeErrorInfo to initializes (optional, can be set to NULL).
 * \param[in] err_line The line where the error occured.
 * \param[in] err_col The column where the error occured.
 */
static void set_deserialize_err_and_clean(FILE* file, DeserializeError err, DeserializeErrorInfo* err_info, unsigned int err_line, unsigned int err_col)
{
	if (err_info != NULL)
		*err_info = (DeserializeErrorInfo){.err = err, .err_line = err_line, .err_col = err_col};
	
	fclose(file);
}

/**
 * \brief Builds a CrossroadConfig from a CrossroadConfigBuilder.
 * \param[out] config The CrossroadConfig which will be builded.
 * \param[in] builder The CrossroadConfigBuilder.
 */
static void build_config(CrossroadConfig* config, CrossroadConfigBuilder const* builder)
{
	config->step_period = builder->step_period;
	config->green_light_dur = builder->green_light_dur * 1000;
	config->yellow_light_dur = builder->yellow_light_dur * 1000;
	config->pedestrian_call_btn_latency = builder->pedestrian_call_btn_latency * 1000;
	config->overload_extra_light_time = builder->overload_extra_light_time * 1000;
	config->overloaded_way_threshold = builder->overloaded_way_threshold;
	config->max_velocities[ENTITY_CAR] = builder->max_car_velocity / 3600.0f;
	config->max_velocities[ENTITY_PEDESTRIAN] = builder->max_pedestrian_velocity / 3600.0f;
	config->time_multiplier = builder->time_multiplier;
	
	for (int i = 0; i < WAY_COUNT; ++i)
	{
		config->car_spawn_probs[i] = builder->cars_per_hour[i] / 3.6e6f * builder->step_period;
		config->pedestrian_spawn_probs[i] = builder->pedestrians_per_hour[i] / 3.6e6f * builder->step_period;
	}
}

DeserializeError crconfig_deserialize(CrossroadConfig* config, char const* config_file_path, DeserializeErrorInfo* err_info)
{
	assert(config != NULL);
	assert(config_file_path != NULL);
	
	CrossroadConfigBuilder builder = {0};	
	
	FILE* file = fopen(config_file_path, "r");
	
	if (!file) // Unable to open file
		return ERR_FILE_NOT_FOUND;
	
	int c;
	char buffer[IO_BUFFER_SIZE];
	unsigned int line = 0;
	
	while ((c = getc(file)) != EOF)
	{
		++line;
		
		if (c == COMMENT_CHAR)
		{
			// Skip the comment line
			do {
				c = getc(file);
			} while (c != EOF && c != '\n');
		}
		else if (isspace(c))
		{
			while (c != EOF && c != '\n' && isspace(c))
				c = getc(file);
			
			if (c != EOF && !isspace(c))
			{
				set_deserialize_err_and_clean(file, ERR_BAD_FORMAT, err_info, line, 1);
				return ERR_BAD_FORMAT;
			}
		}
		else
		{
			int i = 0;
			int name_len;
			
			while (c != EOF && (isalpha(c) || c == '_') && i < IO_BUFFER_SIZE - 1)
			{
				buffer[i++] = c;
				c = getc(file);
			}
			
			buffer[i] = '\0';
			name_len = i;
			
			while (is_blank(c)) // Skip spaces and tabs
			{
				c = getc(file);
				++i;
			}
			
			if (c == EOF || c != '=')
			{
				set_deserialize_err_and_clean(file, ERR_BAD_FORMAT, err_info, line, i + 1);
				return ERR_BAD_FORMAT;
			}
			
			int parse_result = parse_field(&builder, buffer, name_len, file);
			
			if (parse_result != 0)
			{
				set_deserialize_err_and_clean(file, parse_result, err_info, line, i + 1);
				return parse_result;
			}
		}
	}
	
	build_config(config, &builder);
	
	fclose(file);
	return NO_ERROR;
}

void crconfig_print_err(DeserializeErrorInfo const* err_info)
{
	assert(err_info != NULL);
	
	switch (err_info->err)
	{
		case NO_ERROR:
			puts("Aucune erreur.");
			break;
		case ERR_FILE_NOT_FOUND:
			puts("Fichier de configuration inexistant.");
			break;
		case ERR_BAD_FORMAT:
			printf("Format incorrect, ligne %u, colonne %u.\n", err_info->err_line, err_info->err_col);
			break;
		case ERR_BAD_FIELD_NAME:
			printf("Champ inexistant, ligne %u, colonne %u.\n", err_info->err_line, err_info->err_col);
			break;
		case ERR_BAD_FIELD_VALUE:
			printf("Valeur invalide, ligne %u, colonne %u.\n", err_info->err_line, err_info->err_col);
			break;
		default:
			assert(0);
	}
}

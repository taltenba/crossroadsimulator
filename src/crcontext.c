/**
 * \file
 * \brief 	Context of a crossroad simulator.
 * \date    Created:  18/12/2018
 * \date    Modified: 29/12/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#include <assert.h>
#include <pthread.h>
#include <crossed_seg.h>
#include <transform.h>
#include <crcontext.h>

#define START_TRANSFORMS(x, y)	{\
									[WAY_WE] = TRANSFORM_0(x, y),\
									[WAY_SN] = TRANSFORM_90(x, y),\
									[WAY_EW] = TRANSFORM_180(x, y),\
									[WAY_NS] = TRANSFORM_270(x, y)\
								}

int crcontext_init(CrossroadContext* ctx, CrossroadConfig const* config)
{
	assert(ctx != NULL);
	assert(config != NULL);
	
	bool success = pthread_cond_init(&ctx->on_update_cond, NULL) == 0
				&& pthread_mutex_init(&ctx->on_update_mutex, NULL) == 0;
				
	if (!success)
		return -1;
	
	ctx->config = config;
	ctx->is_computing_update = false;
	ctx->traffic_light_colors[AXIS_HORIZONTAL] = COLOR_GREEN;
	ctx->traffic_light_colors[AXIS_VERTICAL] = COLOR_RED;
	ctx->time_before_light_change = config->green_light_dur;
	
	for (int i = 0; i < WAY_COUNT; ++i)
	{
		for (int j = 0; j < ENTITY_TYPE_COUNT; ++j)
			way_init(&ctx->ways[i][j]);
	}
	
	entitypool_init(&ctx->entity_pool);
	
	return 0;
}

static bool crossed_segs_free_when_reached(CrossroadContext* ctx, EntityType way_entity_type, WayId way_id)
{
	int crossed_seg_count;
	CrossedSegment const* crossed_segs = crossedsegs_get(way_entity_type, &crossed_seg_count);
	
	float max_velocity = ctx->config->max_velocities[way_entity_type];
	
	for (int i = 0; i < crossed_seg_count; ++i)
	{
		CrossedSegment const* cr = &crossed_segs[i];
		WayId crossed_way_id = cr->way == WAY_LEFT ? LEFT_WAY(way_id) : RIGHT_WAY(way_id);
		
		if (!way_crossed_seg_free_when_reached(&ctx->ways[crossed_way_id][cr->met_entity_type], cr->end_y, cr->distance_from_crossroad_start, max_velocity))
			return false;
	}
	
	return true;
}

static bool can_entities_cross(CrossroadContext* ctx, EntityType way_entity_type, WayId way_id)
{
	if (ctx->traffic_light_colors[WAY_AXIS(way_id)] != COLOR_GREEN)
		return false;
		
	return crossed_segs_free_when_reached(ctx, way_entity_type, way_id);
}

static void adjust_traffic_light_timer(CrossroadContext* ctx)
{
	CrossroadConfig const* config = ctx->config;
	Axis red_light_axis = ctx->traffic_light_colors[AXIS_HORIZONTAL] == COLOR_RED ? 
							AXIS_HORIZONTAL : AXIS_VERTICAL;
	
	if (ctx->traffic_light_colors[OTHER_AXIS(red_light_axis)] != COLOR_GREEN)
		return;
		
	// If the crossroad is not in intermediary state (ie. there is no yellow light), 
	// then we must check if the current timer must be adjusted according to the 
	// current context.
	
	Way* pedes_way_1 = &ctx->ways[AXIS_WAY_1(red_light_axis)][ENTITY_PEDESTRIAN];
	Way* pedes_way_2 = &ctx->ways[AXIS_WAY_2(red_light_axis)][ENTITY_PEDESTRIAN];
			
	if (!way_is_light_queue_empty(pedes_way_1) || !way_is_light_queue_empty(pedes_way_2))
	{
		// If there is a waiting pedestrian on the red light axis, we must trigger state change 
		// of the traffic lights after some latency.
		
		if (ctx->time_before_light_change > config->pedestrian_call_btn_latency)
			ctx->time_before_light_change = config->pedestrian_call_btn_latency;
	}
	else
	{
		// If no pedestrian is waiting, we must adjust the timer according to the respective
		// car axis load.
		
		Axis green_light_axis = OTHER_AXIS(red_light_axis);
		int overloaded_way_count[AXIS_COUNT] = {0};
		
		for (int i = 0; i < WAY_COUNT; ++i)
		{
			if (ctx->ways[i][ENTITY_CAR].light_queue_size >= config->overloaded_way_threshold)
				overloaded_way_count[WAY_AXIS(i)]++;
		}
		
		if (!ctx->overload_extra_light_time_added && overloaded_way_count[green_light_axis] > 0 
			&& overloaded_way_count[red_light_axis] < WAY_COUNT / AXIS_COUNT)
		{
			// If at least one way of the green-light axis is overloaded and the ways of the other
			// axis not all overloaded, extra green-light must be given provided that no 
			// extra time has already been given to this axis.
			
			ctx->time_before_light_change += config->overload_extra_light_time;
			ctx->overload_extra_light_time_added = true;
		}
		else if (ctx->overload_extra_light_time_added 
			&& overloaded_way_count[red_light_axis] == WAY_COUNT / AXIS_COUNT)
		{
			// If extra green-light time has been given to this axis but the ways of the other 
			// axis are now all overloaded, we must cancel the extra time that was given.
			
			if (ctx->time_before_light_change < config->overload_extra_light_time)
				ctx->time_before_light_change = 0;
			else
				ctx->time_before_light_change -= config->overload_extra_light_time;
				
			ctx->overload_extra_light_time_added = false;
		}
	}
}

static bool update_traffic_light_timer(CrossroadContext* ctx)
{
	adjust_traffic_light_timer(ctx);
	
	unsigned int step_period = ctx->config->step_period;
	
	// Finally, we update the timer according to the time that has elapsed
	// during this simulation step.
	if (ctx->time_before_light_change <= step_period)
		return true;
	
	ctx->time_before_light_change -= step_period;
	return false;
}

static void update_traffic_lights(CrossroadContext* ctx)
{
	if (!update_traffic_light_timer(ctx))
		return;
	
	// Check if an axis has currently yellow lights
	for (int i = 0; i < AXIS_COUNT; ++i)
	{
		if (ctx->traffic_light_colors[i] != COLOR_YELLOW)
			continue;
			
		// If there is yellow lights, those must become red and the other one green.
		ctx->traffic_light_colors[i] = COLOR_RED;
		ctx->traffic_light_colors[OTHER_AXIS(i)] = COLOR_GREEN;
		
		ctx->overload_extra_light_time_added = false;
		ctx->time_before_light_change = ctx->config->green_light_dur;
		
		adjust_traffic_light_timer(ctx);
			
		for (int j = AXIS_WAY_1(OTHER_AXIS(i)); j < WAY_COUNT; j = NEXT_AXIS_WAY(j))
		{
			for (int k = 0; k < ENTITY_TYPE_COUNT; ++k)
			{
				if (can_entities_cross(ctx, k, j))
					way_notify_entities_can_cross(&ctx->ways[j][k]);
			}
		}
		
		return;
	}
	
	// Otherwise, green light must become yellow
	Axis green_light_axis = ctx->traffic_light_colors[AXIS_HORIZONTAL] == COLOR_GREEN ? 
							AXIS_HORIZONTAL : AXIS_VERTICAL;
							
	ctx->traffic_light_colors[green_light_axis] = COLOR_YELLOW;
	ctx->time_before_light_change = ctx->config->yellow_light_dur;
}

bool crcontext_can_entity_cross(CrossroadContext* ctx, Entity const* entity)
{
	assert(ctx != NULL);
	assert(entity != NULL);
	
	return can_entities_cross(ctx, entity->type, entity->cur_way);
}

void crcontext_notify_entity_starts_sleeping_in_light_queue(CrossroadContext* ctx, Entity* entity)
{
	assert(ctx != NULL);
	assert(entity != NULL);
	assert(entity->state == STATE_SLEEPING);
	
	Way* way = &ctx->ways[entity->cur_way][entity->type];
	
	way_lock(way);
	way_notify_entity_starts_sleeping_in_light_queue(&ctx->ways[entity->cur_way][entity->type], entity);
	way_unlock(way);	
}

static int spawn_entity(CrossroadContext* ctx, WayId spawn_way, WayId dest_way, EntityType entity_type, Transform const* start_transforms)
{
	Way* way = &ctx->ways[spawn_way][entity_type];
	
	Entity* entity = entitypool_get(&ctx->entity_pool, entity_type, ctx, &start_transforms[spawn_way], spawn_way, dest_way);
	
	if (entity == NULL)
		return -1;
	
	Entity* prec_entity = way->last_entity;
	
	if (prec_entity != NULL)
	{
		float max_x = entity_get_safe_end_x(prec_entity);
	
		// Check if the spawn location of the entity is free.
		if (entity_get_start_x(entity) > max_x)
			entity->transform.local_pos.x = max_x;
	}
	
	way_notify_entity_spawned(way, entity);
	
	entity_enable(entity);
	return 0;
}

int crcontext_spawn_car(CrossroadContext* ctx, WayId spawn_way, WayId dest_way)
{
	static Transform const start_transforms[WAY_COUNT] 
		= START_TRANSFORMS(MAP_START_X, ROAD_POS_Y);
		
	assert(ctx != NULL);
	assert(dest_way != OPPOSITE_WAY(dest_way)); // Cannot do a U-turn
	
	return spawn_entity(ctx, spawn_way, dest_way, ENTITY_CAR, start_transforms);
}

int crcontext_spawn_pedestrian(CrossroadContext* ctx, WayId spawn_way, WayId dest_way)
{
	static Transform const start_transforms[WAY_COUNT] 
		= START_TRANSFORMS(MAP_START_X, SIDEWALK_POS_Y);
		
	assert(ctx != NULL);
	
	return spawn_entity(ctx, spawn_way, dest_way, ENTITY_PEDESTRIAN, start_transforms);
}

static void check_if_entity_exited_way(CrossroadContext* ctx, Way* way)
{
	Entity* first_way_entity = way->first_entity;
	
	if (first_way_entity == NULL || entity_get_end_x(first_way_entity) < MAP_END_X)
		return;
	
	way_notify_entity_exited(way);
	
	entity_free(first_way_entity);
	entitypool_free(&ctx->entity_pool, first_way_entity);
}

static void check_if_way_crossed_segs_freed(CrossroadContext* ctx, WayId way_id, EntityType way_entity_type)
{
	Way* way = &ctx->ways[way_id][way_entity_type];
	Entity* last_crossing = way->last_crossing_entity;
	
	if (last_crossing == NULL)
		return;
	
	float new_end_x = last_crossing->transform.local_pos.x - entity_get_length(last_crossing);
	
	int crossed_seg_count;
	CrossedSegment const* crossed_segs = crossedsegs_get(way_entity_type, &crossed_seg_count);
	
	int i = crossed_seg_count - 1;
	
	// Crossed_seg are ordered on end_x field, in ascending order
	while (i >= 0 && new_end_x <= crossed_segs[i].end_x)
		--i;
	
	if (i < 0)
		return;
		
	for (; i >= 0; --i)
	{
		EntityType met_entity_type = crossed_segs[i].met_entity_type;
		WayId crossed_way_id = crossed_segs[i].way == WAY_LEFT ? LEFT_WAY(way_id) : RIGHT_WAY(way_id);
		
		if (can_entities_cross(ctx, met_entity_type, crossed_way_id))
			way_notify_entities_can_cross(&ctx->ways[crossed_way_id][met_entity_type]);
	}	
}

static void update_entity_way(CrossroadContext* ctx, EntityType entity_type, WayId way_id)
{
	Way* way = &ctx->ways[way_id][entity_type];
	
	way_update_next_entity_to_cross(way);
	
	check_if_way_crossed_segs_freed(ctx, way_id, entity_type);
	
	if (way->first_crossing_entity != NULL && entity_has_passed_crossroad_end(way->first_crossing_entity))
		way_notify_entity_crossed(way);
	
	check_if_entity_exited_way(ctx, way);
}

static void update_ways(CrossroadContext* ctx)
{
	for (int i = 0; i < WAY_COUNT; ++i)
	{
		for (int j = 0; j < ENTITY_TYPE_COUNT; ++j)
			update_entity_way(ctx, j, i);
	}
}

void crcontext_compute_update_async(CrossroadContext* ctx)
{
	assert(ctx != NULL);
	
	// Only entities that are not inactive, waiting for a condition to occur will be waken up.
	unsigned int active_entity_count = 0;
	
	for (int i = 0; i < WAY_COUNT; ++i)
	{
		for (int j = 0; j < ENTITY_TYPE_COUNT; ++j)
			active_entity_count += ctx->ways[i][j].active_entity_count;
	}
	
	pthread_barrier_init(&ctx->update_step_completed_barrier, NULL, active_entity_count + 1);
	
	pthread_mutex_lock(&ctx->on_update_mutex);
	
	ctx->is_computing_update = true;
	pthread_cond_broadcast(&ctx->on_update_cond); // Wakes up all active entities.
	
	pthread_mutex_unlock(&ctx->on_update_mutex);
}

void crcontext_apply_update_sync(CrossroadContext* ctx)
{
	assert(ctx != NULL);
	
	// Waits all entities have computed their updated state.
	pthread_barrier_wait(&ctx->update_step_completed_barrier);
	
	ctx->is_computing_update = false;
	
	// Waits all entities have updated their state.
	pthread_barrier_wait(&ctx->update_step_completed_barrier);
	
	// Update state of the ways.
	update_ways(ctx);
	
	// Update state of the traffic lights.
	update_traffic_lights(ctx);
	
	pthread_barrier_destroy(&ctx->update_step_completed_barrier);
}

static void waiting_entity_cleanup_handler(void* lock)
{
	pthread_mutex_unlock((pthread_mutex_t*) lock);
}

void crcontext_wait_compute_update(CrossroadContext* ctx)
{
	assert(ctx != NULL);
	
	pthread_mutex_lock(&ctx->on_update_mutex);
	
	pthread_cleanup_push(waiting_entity_cleanup_handler, &ctx->on_update_mutex);
	
	while (!ctx->is_computing_update)
		pthread_cond_wait(&ctx->on_update_cond, &ctx->on_update_mutex);
	
	pthread_cleanup_pop(0);
	
	pthread_mutex_unlock(&ctx->on_update_mutex);
}

void crcontext_wait_next_update_step(CrossroadContext* ctx)
{
	assert(ctx != NULL);
	pthread_barrier_wait(&ctx->update_step_completed_barrier);
}

void crcontext_free(CrossroadContext* ctx)
{
	assert(ctx != NULL);
	
	for (int i = 0; i < WAY_COUNT; ++i)
	{
		for (int j = 0; j < ENTITY_TYPE_COUNT; ++j)
		{
			Entity* cur = ctx->ways[i][j].last_entity;
			Entity* prec;
			
			while (cur != NULL)
			{
				prec = cur->prec_entity;
				entity_free(cur);
				cur = prec;
			}
		}
	}
	
	entitypool_clear(&ctx->entity_pool);
	
	pthread_cond_destroy(&ctx->on_update_cond);
	pthread_mutex_destroy(&ctx->on_update_mutex);
}

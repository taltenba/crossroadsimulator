/**
 * \file
 * \brief 	GUI for a crossroad simulator.
 * \date    Created:  23/12/2018
 * \date    Modified: 25/12/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#include <stdlib.h>
#include <assert.h>
#include <crossroad.h>
#include <crgui.h>

#define WINDOW_TITLE "CrossroadSimulator"       /**< The GUI window title */

#define PIXS_PER_M 16                           /**< The number of pixel corresponding to one meter in the simulated environnement */
#define WINDOW_WIDTH (MAP_WIDTH * PIXS_PER_M)   /**< The window width in pixels */
#define WINDOW_HEIGHT (MAP_HEIGHT * PIXS_PER_M) /**< THe window height in pixels */
#define WINDOW_HALF_WIDTH (WINDOW_WIDTH / 2)
#define WINDOW_HALF_HEIGHT (WINDOW_HEIGHT / 2)
#define TO_WIN_X(x) ((x) * PIXS_PER_M + WINDOW_HALF_WIDTH)   /**< Converts an x-value from the simulated space to an x-value on the window */
#define TO_WIN_Y(y) (-(y) * PIXS_PER_M + WINDOW_HALF_HEIGHT) /**< Converts an y-value from the simulated space to an y-value on the window */

#define LIGHT_WIDTH 8                           /**< Width of the traffic lights in pixels */
#define LIGHT_POS(x, y)	{\
							[WAY_WE] = {TO_WIN_X(x) - LIGHT_WIDTH, TO_WIN_Y(y)},\
							[WAY_SN] = {TO_WIN_X(-(y)), TO_WIN_Y(x) + LIGHT_WIDTH},\
							[WAY_EW] = {TO_WIN_X(-(x)) + LIGHT_WIDTH, TO_WIN_Y(-(y))},\
							[WAY_NS] = {TO_WIN_X(y), TO_WIN_Y(-(x)) - LIGHT_WIDTH}\
						}
						
#define DEFAULT_UPDATE_WAIT_TIME 100            /**< Default time between two GUI event checks */

static SDL_Texture* load_bmp_texture(CrossroadGui* gui, char* path)
{
	SDL_Surface* surf = SDL_LoadBMP(path);
	
	if (surf == NULL)
		return NULL;
	
	SDL_Texture* texture = SDL_CreateTextureFromSurface(gui->renderer, surf);
	SDL_FreeSurface(surf);
	
	return texture;
}

static bool is_init_successful(CrossroadGui const* gui)
{
	if (gui->window == NULL || gui->renderer == NULL || gui->background == NULL)
		return false;
		
	for (int i = 0; i < ENTITY_TYPE_COUNT; ++i)
	{
		if (gui->entities[i] == NULL)
			return false;
	}
	
	for (int i = 0; i < LIGHT_COLOR_COUNT; ++i)
	{
		if (gui->lights[i] == NULL)
			return false;
	}
	
	return true;
}

static void clean_failed_init(CrossroadGui* gui)
{
	if (gui->background != NULL)
		SDL_DestroyTexture(gui->background);
	
	for (int i = 0; i < ENTITY_TYPE_COUNT; ++i)
	{
		if (gui->entities[i] != NULL)
			SDL_DestroyTexture(gui->entities[i]);
	}
	
	for (int i = 0; i < LIGHT_COLOR_COUNT; ++i)
	{
		if (gui->lights[i] != NULL)
			SDL_DestroyTexture(gui->lights[i]);
	}
	
	if (gui->renderer != NULL)
		SDL_DestroyRenderer(gui->renderer);
		
	if (gui->window != NULL)
		SDL_DestroyWindow(gui->window);
	
	SDL_Quit();
}

int crgui_init(CrossroadGui* gui, CrossroadSimulator* sim)
{
	assert(gui != NULL);
	assert(sim != NULL);
	
	if (SDL_Init(SDL_INIT_VIDEO) != 0)
		return -1;
	
	gui->window = SDL_CreateWindow(WINDOW_TITLE, 
					SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 
					WINDOW_WIDTH, WINDOW_HEIGHT,
					0);
	
	gui->renderer = SDL_CreateRenderer(gui->window, -1, SDL_RENDERER_PRESENTVSYNC);
	
	gui->background = load_bmp_texture(gui, "res/background.bmp");
	gui->entities[ENTITY_CAR] = load_bmp_texture(gui, "res/car.bmp");
	gui->entities[ENTITY_PEDESTRIAN] = load_bmp_texture(gui, "res/pedestrian.bmp");
	gui->lights[COLOR_GREEN] = load_bmp_texture(gui, "res/light_green.bmp");
	gui->lights[COLOR_YELLOW] = load_bmp_texture(gui, "res/light_yellow.bmp");
	gui->lights[COLOR_RED] = load_bmp_texture(gui, "res/light_red.bmp");
	
	if (!is_init_successful(gui))
	{
		clean_failed_init(gui);
		return -1;
	}
	
	gui->sim = sim;
	
	return 0;
}

static void render_lights(CrossroadGui* gui)
{
	static Position const light_pos[ENTITY_TYPE_COUNT][WAY_COUNT] = 
			{
				[ENTITY_CAR] = 
						LIGHT_POS(CAR_WAY_1ST_CROSSWALK_START_X, 0),
				[ENTITY_PEDESTRIAN] =
						LIGHT_POS(PEDESTRIAN_WAY_CROSSROAD_START_X, 
							CAR_WAY_1ST_CROSSWALK_END_X)
			};
			
	LightColor* light_colors = gui->sim->ctx.traffic_light_colors;
	SDL_Rect pos;
	SDL_Point rot_center = {0, 0};
	float rot = 0.0f;
	
	SDL_QueryTexture(gui->lights[0], NULL, NULL, &pos.w, &pos.h);
	
	for (int i = 0; i < WAY_COUNT; ++i)
	{
		Axis axis = WAY_AXIS(i);
		
		for (int j = 0; j < ENTITY_TYPE_COUNT; ++j)
		{		
			pos.x = light_pos[j][i].x;
			pos.y = light_pos[j][i].y;
			
			LightColor color;
			
			switch (j)
			{
				case ENTITY_CAR:
					color = light_colors[axis];
					break;
				case ENTITY_PEDESTRIAN:
					color = light_colors[axis] == COLOR_GREEN ?
							COLOR_GREEN : COLOR_RED;
					break;
			}
			
			SDL_RenderCopyEx(gui->renderer, gui->lights[color], NULL, &pos, 
				rot, &rot_center, SDL_FLIP_NONE);
		}
		
		rot -= 90.0f;
	}
}

static void render_entities(CrossroadGui* gui)
{
	for (int i = 0; i < ENTITY_TYPE_COUNT; ++i)
	{
		SDL_Rect pos;
		SDL_Texture* texture = gui->entities[i];
		float rot = 0.0f;
		
		SDL_QueryTexture(texture, NULL, NULL, &pos.w, &pos.h);
		
		SDL_Point rot_center;
		rot_center.x = pos.w;
		rot_center.y = 0;
		
		for (int j = 0; j < WAY_COUNT; ++j)
		{
			Way* way = &gui->sim->ctx.ways[j][i];
			Entity* cur = way->last_entity;
			
			SDL_Point offset;
			offset.x = -pos.w;
			offset.y = 0;
			
			while (cur != NULL)
			{
				Position world_pos;
				transform_get_world_pos(&cur->transform, &world_pos);
				
				pos.x = TO_WIN_X(world_pos.x) + offset.x;
				pos.y = TO_WIN_Y(world_pos.y) + offset.y;
				
				SDL_RenderCopyEx(gui->renderer, texture, NULL, &pos, 
						rot, &rot_center, SDL_FLIP_NONE);
						
				cur = cur->prec_entity;
			}
			
			rot -= 90.0f;
		}
	}
}

static void update_gui(CrossroadGui* gui)
{
	SDL_RenderCopy(gui->renderer, gui->background, NULL, NULL);
	
	render_lights(gui);
	render_entities(gui);
	
	SDL_RenderPresent(gui->renderer);
}

int crgui_run(CrossroadGui* gui)
{
	assert(gui != NULL);
	
	if (crsim_start(gui->sim) != 0)
		return -1;
	
	unsigned int update_wait_time = DEFAULT_UPDATE_WAIT_TIME;
	
	if (gui->sim->config.step_period < update_wait_time)
		update_wait_time = gui->sim->config.step_period;
	
	bool running = true;
	
	do
	{
		if (crsim_timedwait_update(gui->sim, update_wait_time) == 0)
		{
			update_gui(gui);
		
			crsim_notify_update_processed(gui->sim);
		}
		
		SDL_Event event;
		
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
				running = false;
		}
	} while (running);
	
	crsim_stop(gui->sim);
	
	return 0;
}

void crgui_free(CrossroadGui* gui)
{
	assert(gui != NULL);
	
	SDL_DestroyTexture(gui->background);
	
	for (int i = 0; i < ENTITY_TYPE_COUNT; ++i)
		SDL_DestroyTexture(gui->entities[i]);

	for (int i = 0; i < LIGHT_COLOR_COUNT; ++i)
		SDL_DestroyTexture(gui->lights[i]);
		
	SDL_DestroyRenderer(gui->renderer);
	SDL_DestroyWindow(gui->window);
	
	SDL_Quit();
}

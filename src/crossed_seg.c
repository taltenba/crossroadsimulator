/**
 * \file
 * \brief 	Crossed segments of the crossroad (ie. way segments that are crossed by another way).
 * \date    Created:  18/12/2018
 * \date    Modified: 18/12/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#include <stdlib.h>
#include <crossroad.h>
#include <assert.h>
#include <crossed_seg.h>
		
CrossedSegment const CAR_CROSSED_SEGS[CAR_CROSSED_SEG_COUNT] = 
		{
			{WAY_LEFT, ENTITY_PEDESTRIAN, 0.0f, CAR_WAY_1ST_CROSSWALK_END_X, PEDESTRIAN_WAY_CROSSROAD_END_X},
			{WAY_LEFT, ENTITY_CAR, CROSSWALK_WIDTH, CAR_WAY_CROSSROAD_MID_X, CAR_WAY_2ND_CROSSWALK_START_X + CROSSWALK_OFFSET},
			{WAY_RIGHT, ENTITY_CAR, CAR_WAY_CROSSROAD_HALF_LENGTH, CAR_WAY_2ND_CROSSWALK_START_X, CAR_WAY_CROSSROAD_MID_X},
			{WAY_RIGHT, ENTITY_PEDESTRIAN, CAR_WAY_CROSSROAD_LENGTH - CROSSWALK_WIDTH, CAR_WAY_CROSSROAD_END_X, PEDESTRIAN_WAY_CROSSROAD_MID_X}
		};
		
CrossedSegment const PEDESTRIAN_CROSSED_SEGS[PEDESTRIAN_CROSSED_SEG_COUNT] = 
		{
			{WAY_LEFT, ENTITY_CAR, 0.0f, PEDESTRIAN_WAY_CROSSROAD_MID_X, CAR_WAY_CROSSROAD_END_X},
			{WAY_RIGHT, ENTITY_CAR, CROSSWALK_HALF_LENGTH, CAR_WAY_1ST_CROSSWALK_END_X},
		};
		
CrossedSegment const* crossedsegs_get(EntityType type, int* count)
{
	switch (type)
	{
		case ENTITY_CAR:
			*count = CAR_CROSSED_SEG_COUNT;
			return CAR_CROSSED_SEGS;
		case ENTITY_PEDESTRIAN:
			*count = PEDESTRIAN_CROSSED_SEG_COUNT;
			return PEDESTRIAN_CROSSED_SEGS;
		default:
			assert(0);
	}
	
	return NULL;
}

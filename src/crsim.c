/**
 * \file
 * \brief 	Simulator of a crossroad (4-way intersection) with traffic lights and crosswalks.
 * \date    Created:  10/12/2018
 * \date    Modified: 20/12/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <pthread.h>
#include <assert.h>
#include <crconfig.h>
#include <rnd_util.h>
#include <time.h>
#include <crsim.h>

/**
 * \brief Randomly populates the simulation context with entities.
 * \param[in] sim The simulator.
 */
static void populate(CrossroadSimulator* sim)
{
	CrossroadConfig* config = &sim->config;
	
	// TODO: allow to configure stdev
	for (int i = 0; i < WAY_COUNT; ++i)
	{
		if (randf() < config->car_spawn_probs[i])
			crcontext_spawn_car(&sim->ctx, i, i);
		
		if (randf() < config->pedestrian_spawn_probs[i])
			crcontext_spawn_pedestrian(&sim->ctx, i, i);
	}
}

static void sim_thread_cleanup_handler(void* arg)
{
	CrossroadSimulator* sim = (CrossroadSimulator*) arg;
	
	// When simulation thread is canceled, finish the current update
	// to properly release the synchronization barriers.
	crcontext_apply_update_sync(&sim->ctx);
}

/**
 * \brief Runs the crossroad simulation.
 * \param[in] arg A pointer to the simulator (CrossroadSimulator*).
 * \return PTHREAD_CANCELED when the simulation thread is canceled.
 *
 * The sole way to stop this routine is to cancel the simulation thread.
 */
static void* sim_routine(void* arg)
{
	CrossroadSimulator* sim = (CrossroadSimulator*) arg;
	CrossroadContext* ctx = &sim->ctx;
	useconds_t step_period_us = sim->config.step_period * 1000 / sim->config.time_multiplier;
	
	pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
	pthread_cleanup_push(sim_thread_cleanup_handler, sim);
	
	while (1)
	{
		populate(sim);
		
		// Notifies the simulation observer that the crossroad context has been updated.
		sem_post(&sim->ctx_updated_sem);
		
		// Starts the computation of the next update.
		crcontext_compute_update_async(ctx);
		
		usleep(step_period_us);
		
		pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
		
		// Ensure that the simulation observer has finished to process the last
		// crossroad context update.
		sem_wait(&sim->ctx_update_proccessed);
		
		pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
		
		// Effectively update the context.
		crcontext_apply_update_sync(ctx);
	}
	
	pthread_cleanup_pop(0);
	
	return (void*) EXIT_SUCCESS;
}

CrossroadSimulator* crsim_new(char const* config_file_path, DeserializeErrorInfo* err_info)
{
	assert(config_file_path != NULL);
	
	err_info->err = NO_ERROR;
	
	CrossroadSimulator* sim = malloc(sizeof(CrossroadSimulator));
	
	if (!sim)
		return NULL;
	
	if (crconfig_deserialize(&sim->config, config_file_path, err_info) != 0) // Unable to deserialize the file
	{
		free(sim);
		return NULL;
	}
	
	sim->is_running = false;
	
	sem_init(&sim->ctx_updated_sem, 0, 0);
	sem_init(&sim->ctx_update_proccessed, 0, 0);

	return sim;
}

int crsim_start(CrossroadSimulator* sim)
{
	assert(sim != NULL);
	
	if (sim->is_running) // Already running
		return 0;
	
	bool success = crcontext_init(&sim->ctx, &sim->config) == 0
				&& pthread_create(&sim->sim_thread, NULL, sim_routine, sim) == 0;
	
	if (!success)
		return -1;
	
	sim->is_running = true;
	return 0;	
}

void crsim_stop(CrossroadSimulator* sim)
{
	assert(sim != NULL);
	
	if (!sim->is_running)
		return;
	
	sim->is_running = false;
	pthread_cancel(sim->sim_thread);
	
	pthread_join(sim->sim_thread, NULL);
	
	crcontext_free(&sim->ctx);
}

int crsim_timedwait_update(CrossroadSimulator* sim, unsigned int ms)
{
	assert(sim != NULL);
	
	struct timespec abs_timeout;
	clock_gettime(CLOCK_REALTIME, &abs_timeout);
	
	unsigned int sec = ms / 1000;
	abs_timeout.tv_sec += sec;
	abs_timeout.tv_nsec += (ms - sec * 1000) * 1e6;
	
	return sem_timedwait(&sim->ctx_updated_sem, &abs_timeout);
}

void crsim_notify_update_processed(CrossroadSimulator* sim)
{
	assert(sim != NULL);
	sem_post(&sim->ctx_update_proccessed);
}

void crsim_destroy(CrossroadSimulator* sim)
{
	assert(sim != NULL);
	
	if (sim->is_running)
		crsim_stop(sim);
	
	sem_destroy(&sim->ctx_updated_sem);
	sem_destroy(&sim->ctx_update_proccessed);
	free(sim);
}


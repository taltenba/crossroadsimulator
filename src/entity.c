/**
 * \file
 * \brief 	Entity of a crossroad environnement (car or pedestrian).
 * \date    Created:  18/12/2018
 * \date    Modified: 27/12/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */
 
#include <stdlib.h>
#include <stdbool.h>
#include <crcontext.h>
#include <assert.h>
#include <pthread.h>
#include <entity.h>

void entity_init(Entity* entity, EntityType type, CrossroadContext* ctx, Transform const* transform, WayId src_way, WayId dest_way)
{
	assert(entity != NULL);
	assert(ctx != NULL);
	assert(transform != NULL);

	entity->ctx = ctx;
	entity->type = type;
	entity->state = STATE_DISABLED;
	entity->transform = *transform;
	entity->cur_way = src_way;
	entity->dest_way = dest_way;
	entity->velocity = 0.0f;
	entity->is_in_light_queue = false;
	entity->succ_entity = NULL;
	entity->prec_entity = NULL;
	
	sem_init(&entity->can_move_sem, 0, 0);
}

static bool has_moved(float velocity)
{
	return velocity > 0.000001f;
}

static float get_crossroad_start_x(Entity const* entity)
{
	switch (entity->type)
	{
		case ENTITY_CAR:
			return CAR_WAY_CROSSROAD_START_X;
		case ENTITY_PEDESTRIAN:
			return PEDESTRIAN_WAY_CROSSROAD_START_X;
		default:
			assert(0);
	}
	
	return -1.0f;
}

static void* entity_routine(void* arg)
{
	Entity* entity = (Entity*) arg;
	CrossroadContext* ctx = entity->ctx;
	float crossroad_start_x = get_crossroad_start_x(entity);
	float max_velocity = entity_get_max_velocity(entity);
	float step_period = ctx->config->step_period;
	float max_delta_x = max_velocity * step_period;
	
	while (1)
	{
		/* If the entity is waiting in the light queue, wait until the entity can move */
		if (entity->state == STATE_SLEEPING)
			sem_wait(&entity->can_move_sem);
		
		/* First: Compute update */
		crcontext_wait_compute_update(ctx);
		
		pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
		
		EntityState new_state = STATE_AWAKE;
		float cur_x = entity->transform.local_pos.x;
		float new_x = cur_x + max_delta_x;
		float new_vel;
		
		if (entity->prec_entity != NULL)
		{		
			float max_x = entity_get_safe_end_x(entity->prec_entity);
			
			if (new_x >= max_x)
			{
				// Entities do not move back
				new_x = max_x < cur_x ? cur_x : max_x;
				
				if (entity->prec_entity->state == STATE_SLEEPING)
					new_state = STATE_FALLING_ASLEEP;
			}
		}
		
		if (cur_x <= crossroad_start_x && new_x > crossroad_start_x && !crcontext_can_entity_cross(ctx, entity))
		{
			new_x = crossroad_start_x;
			new_state = STATE_FALLING_ASLEEP;
		}
		
		new_vel = (new_x - cur_x) / step_period;
		
		if (new_state == STATE_FALLING_ASLEEP && !has_moved(new_vel))
			new_state = STATE_SLEEPING;
		
		/* Second: effectively apply update */
		crcontext_wait_next_update_step(ctx);
		
		entity->transform.local_pos.x = new_x;
		entity->state = new_state;
		entity->velocity = new_vel;
		
		if (new_state == STATE_SLEEPING)
			crcontext_notify_entity_starts_sleeping_in_light_queue(ctx, entity);
		
		/* Third: wait full context update is over */
		crcontext_wait_next_update_step(ctx);
		
		pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	}
	
	return (void*) EXIT_SUCCESS;
}

void entity_enable(Entity* entity)
{
	assert(entity != NULL);
	
	if (entity->state != STATE_DISABLED)
		return;
		
	entity->state = STATE_AWAKE;
	pthread_create(&entity->thread, NULL, entity_routine, entity);
}

void entity_disable(Entity* entity)
{
	assert(entity != NULL);
	
	if (entity->state == STATE_DISABLED)
		return;
	
	pthread_cancel(entity->thread);
	pthread_join(entity->thread, NULL);
	
	entity->state = STATE_DISABLED;
}

void entity_wake_up(Entity* entity)
{
	assert(entity != NULL);
	assert(entity->state == STATE_SLEEPING);
	
	entity->state = STATE_AWAKE;
	sem_post(&entity->can_move_sem);
}

float entity_get_max_velocity(Entity const* entity)
{
	assert(entity != NULL);
	return entity->ctx->config->max_velocities[entity->type];
}

float entity_get_length(Entity const* entity)
{
	assert(entity != NULL);
	
	switch (entity->type)
	{
		case ENTITY_CAR:
			return CAR_LENGTH;
		case ENTITY_PEDESTRIAN:
			return PEDESTRIAN_LENGTH;
		default:
			assert(0);
	}
	
	return -1.0f;
}

float entity_get_safe_distance(Entity const* entity)
{
	assert(entity != NULL);
	
	switch (entity->type)
	{
		case ENTITY_CAR:
			return entity_has_moved(entity) ?
				MIN_MOVING_CAR_DISTANCE : MIN_STATIONARY_CAR_DISTANCE;
		case ENTITY_PEDESTRIAN:
			return MIN_PEDESTRIAN_DISTANCE;
		default:
			assert(0);
	}
	
	return -1.0f;
}

float entity_get_start_x(Entity const* entity)
{
	assert(entity != NULL);
	return entity->transform.local_pos.x;
}

float entity_get_end_x(Entity const* entity)
{
	assert(entity != NULL);
	return entity->transform.local_pos.x - entity_get_length(entity);
}

float entity_get_safe_end_x(Entity const* entity)
{
	assert(entity != NULL);
	return entity_get_end_x(entity) - entity_get_safe_distance(entity);
}

bool entity_has_moved(Entity const* entity)
{
	assert(entity != NULL);
	return has_moved(entity->velocity);
}

bool entity_has_passed_crossroad_start(Entity const* entity)
{
	assert(entity != NULL);
	return entity->transform.local_pos.x > get_crossroad_start_x(entity);
}

bool entity_has_passed_crossroad_end(Entity const* entity)
{
	assert(entity != NULL);
	
	float end_x = entity_get_end_x(entity);
	
	switch (entity->type)
	{
		case ENTITY_CAR:
			return end_x > CAR_WAY_CROSSROAD_END_X;
		case ENTITY_PEDESTRIAN:
			return end_x > PEDESTRIAN_WAY_CROSSROAD_END_X;
		default:
			assert(0);
	}
	
	return false;
}

void entity_free(Entity* entity)
{
	assert(entity != NULL);
	
	entity_disable(entity);
}

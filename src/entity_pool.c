/**
 * \file
 * \brief 	Pool of entities.
 * \date    Created:  18/12/2018
 * \date    Modified: 18/12/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */
 
#include <stdlib.h>
#include <assert.h>
#include <entity_pool.h>

#define ENTITIES_PER_MEM_CHUNK 64   /**< Number of entities per pool chunk */

void entitypool_init(EntityPool* pool)
{
	assert(pool != NULL);
	
	fsmpool_init(pool, sizeof(Entity), ENTITIES_PER_MEM_CHUNK, ENTITIES_PER_MEM_CHUNK);
}

Entity* entitypool_get(EntityPool* pool, EntityType type, struct crcontext* ctx, Transform const* transform, WayId src_way, WayId dest_way)
{
	assert(pool != NULL);
	
	Entity* entity = fsmpool_alloc(pool);
	
	if (entity == NULL)
		return NULL;
		
	entity_init(entity, type, ctx, transform, src_way, dest_way);
	
	return entity;
}

void entitypool_free(EntityPool* pool, Entity* entity)
{
	assert(pool != NULL);
	assert(entity != NULL);
	
	fsmpool_free(pool, entity);
}

void entitypool_clear(EntityPool* pool)
{
	assert(pool != NULL);
	
	fsmpool_clear(pool);
}

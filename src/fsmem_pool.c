/**
 * \file
 * \brief 	Fixed-size memory pool
 * \date 	Created: 09/05/2017
 * \date	Modified: 18/11/2017
 * \author 	ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 *
 * This memory pool enables dynamic allocation of fixed-size memory blocks.
 *
 * These blocks are stored in bigger chunks, allocated with only once malloc call.
 *
 * The use of such a memory pool reduces then the memory overheads due to the allocation of a great deal of 
 * small memory blocks using malloc. Minimizing malloc calls, the memory pool also highly increase the time 
 * efficiency.
 *
 * Last but not least, blocks are stored continiously in a chunk which enhances the spatial locality 
 * of memory and is therefore more cache-friendly, in particular if few or no blocks are freed before the 
 * free of the whole pool.
 */

#include <stdlib.h>
#include <assert.h>
#include <fsmem_pool.h>

union fsblock
{
    FSMemBlock* next;     	/**< Pointer to the next free block when the block is in the free list */
    char data[1];           /**< First byte of the block */
};

struct fschunk
{
    FSMemChunk* previous;     /**< Pointer to the previous chunk */
	FSMemBlock blocks[1];	  /**< First memory block in the chunk */
};

/**
 * \brief Align a number on the pointer size
 * \param[in] n Number to align (n > 0)
 * \return The given number aligned on the pointer size, 
 * 		   that is to say the smallest multiple of sizeof(void*) greater or equal to n
 *
 * A call with n = 0 will return sizeof(void*), not 0.
 */
static size_t align_on_ptr(size_t n)
{
    return ((n - 1) / sizeof(void*) + 1) * sizeof(void*);
}

/**
 * \brief Allocate a new memory chunk in a memory pool
 * \param[in] pool The memory pool
 * \param[in] chunk_size Size of the chunk to allocate
 * \return 0 on success, -1 otherwise (out of memory)
 */
static int new_chunk(FSMemPool* pool, size_t chunk_size)
{
    FSMemChunk* chunk = malloc(chunk_size);

    if (chunk == NULL)
        return -1;

    chunk->previous = pool->cur_chunk;

    pool->cur_chunk = chunk;
    pool->next_block = chunk->blocks;
    pool->last_block = (FSMemBlock*) ((char*) chunk + chunk_size);

    return 0;
}

void fsmpool_init(FSMemPool* pool, size_t block_size, size_t chunk_capacity, size_t init_capacity)
{
	assert(chunk_capacity > 0);
	
	if (block_size < sizeof(FSMemBlock))
		block_size = sizeof(FSMemBlock);
	
	/* Ensure that block_size is aligned on sizeof(void*) to avoid at least the most common alignement problems */
	block_size = align_on_ptr(block_size);	
	
    pool->cur_chunk = NULL;
	pool->next_block = NULL;
	pool->last_block = NULL;
    pool->free_blocks = NULL;
    pool->chunk_size = block_size * chunk_capacity + sizeof(FSMemChunk*);	/* Add sizeof(MemChunk*) to keep memory for the "previous" field of the MemChunk struct */
    pool->block_size = block_size;

	if (init_capacity != 0)
		new_chunk(pool, block_size * init_capacity + sizeof(FSMemChunk*));
}

void* fsmpool_alloc(FSMemPool* pool)
{
	FSMemBlock* block;
	
    assert(pool != NULL);
	
    if (pool->free_blocks != NULL)
    {
        block = pool->free_blocks;
        pool->free_blocks = block->next;

        return block;
    }

	/* If there is no chunk in the pool or the actual chunk ran out of memory, we allocate a new one */
    if (pool->next_block == NULL || pool->next_block >= pool->last_block)
    {
        if (new_chunk(pool, pool->chunk_size) != 0)
            return NULL;
    }

    block = pool->next_block;

    pool->next_block = (FSMemBlock*) (block->data + pool->block_size);

    return block;
}

void fsmpool_free(FSMemPool* pool, void* block)
{
	FSMemBlock* free_block;
	
    assert(pool != NULL);
    assert(block != NULL);

	/* The content of the block is no more used so we use this memory to store
	 * the address of the current first memory block in the free list */
    free_block = (FSMemBlock*) block;       			
    free_block->next = pool->free_blocks;

    pool->free_blocks = free_block;
}

void fsmpool_clear(FSMemPool* pool)
{
	FSMemChunk* previous;
    FSMemChunk* cur;
    
	assert(pool != NULL);
	
	cur = pool->cur_chunk;

    while (cur != NULL)
    {
        previous = cur->previous;
        free(cur);
        cur = previous;
    }
	
	pool->cur_chunk = NULL;
	pool->next_block = NULL;
	pool->last_block = NULL;
    pool->free_blocks = NULL;
}

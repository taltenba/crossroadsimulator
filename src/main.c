/**
 * \file
 * \brief 	Simulator of a crossroad with SDL GUI.
 * \date    Created:  23/12/2018
 * \date    Modified: 23/12/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <crsim.h>
#include <crgui.h>

int main(int argc, char** argv)
{
	srand(time(NULL));
	
	if (argc != 2)
	{
		puts("Erreur : veuillez indiquer en argument le chemin vers le fichier de configuration.");
		return EXIT_FAILURE;
	}
	
	DeserializeErrorInfo err_info;
	CrossroadSimulator* sim = crsim_new(argv[1], &err_info);
	
	if (sim == NULL)
	{
		puts("Erreur : Impossible d'initialiser le simulateur.");
		crconfig_print_err(&err_info);
		return EXIT_FAILURE;
	}
	
	CrossroadGui gui;
	
	if (crgui_init(&gui, sim) != 0)
	{
		puts("Erreur: Impossible d'initialiser l'interface graphique.");
		crsim_destroy(sim);
		return EXIT_FAILURE;
	}
	
	if (crgui_run(&gui) != 0)
	{
		puts("Erreur : Impossible de démarrer la simulation.");
		crgui_free(&gui);
		crsim_destroy(sim);
		return EXIT_FAILURE;
	}
	
	crgui_free(&gui);
	crsim_destroy(sim);
	
	return EXIT_SUCCESS;
}

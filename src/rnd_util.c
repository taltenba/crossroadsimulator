/**
 * \file
 * \brief   Utility routines related to pseudo-random number generation.
 * \date    Created:  20/05/2017
 * \date    Modified: 20/05/2017
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */
 
#include <rnd_util.h>
 
extern float randf();


/**
 * \file
 * \brief 	Routines and structures to deal with the position and rotation of objects.
 * \date    Created:  19/12/2018
 * \date    Modified: 19/12/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <transform.h>

void transform_init(Transform* transform, float local_pos_x, float local_pos_y, float rot)
{
	assert(transform != NULL);
	
	transform->local_pos.x = local_pos_x;
	transform->local_pos.y = local_pos_y;
	
	transform_set_rot(transform, rot);
}

void transform_set_rot(Transform* transform, float rot)
{
	assert(transform != NULL);
	
	float cos_rot = cosf(rot);
	float sin_rot = sinf(rot);
	
	transform->local_to_world_matrix[0][0] = cos_rot;
	transform->local_to_world_matrix[0][1] = -sin_rot;
	transform->local_to_world_matrix[1][0] = sin_rot;
	transform->local_to_world_matrix[1][1] = cos_rot;
	
	//transform->rot = rot;
}

void transform_translate(Transform* transform, float delta_x, float delta_y)
{
	assert(transform != NULL);
	
	transform->local_pos.x += delta_x;
	transform->local_pos.y += delta_y;
}

void transform_get_world_pos(Transform const* transform, Position* world_pos)
{
	assert(transform != NULL);
	assert(world_pos != NULL);
	
	float x = transform->local_pos.x;
	float y = transform->local_pos.y;
	float const (*m)[2] = transform->local_to_world_matrix;
	
	world_pos->x = x * m[0][0] + y * m[0][1];
	world_pos->y = x * m[1][0] + y * m[1][1];
}

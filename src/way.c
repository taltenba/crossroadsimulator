/**
 * \file
 * \brief 	Way of a crossroad.
 * \date    Created:  18/12/2018
 * \date    Modified: 29/12/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#include <way.h>
#include <assert.h>

void way_init(Way* way)
{
	assert(way != NULL);
	
	way->first_entity = NULL;
	way->last_entity = NULL;
	way->next_entity_to_cross = NULL;
	way->last_waken_up_queue_entity = NULL;
	way->first_crossing_entity = NULL;
	way->last_crossing_entity = NULL;
	way->light_queue_size = 0;
	way->active_entity_count = 0;
	
	pthread_mutex_init(&way->mutex, NULL);
}

void way_lock(Way* way)
{
	assert(way != NULL);
	pthread_mutex_lock(&way->mutex);
}

void way_unlock(Way* way)
{
	assert(way != NULL);
	pthread_mutex_unlock(&way->mutex);
}

void way_notify_entity_spawned(Way* way, Entity* entity)
{
	assert(way != NULL);
	assert(entity != NULL);
	
	Entity* old_last = way->last_entity;
	
	way->last_entity = entity;
	entity->prec_entity = old_last;
	
	if (old_last == NULL)
		way->first_entity = entity;
	else
		old_last->succ_entity = entity;
	
	if (way->next_entity_to_cross == NULL)
		way->next_entity_to_cross = entity;
	
	way->active_entity_count++;
}

void way_notify_entity_exited(Way* way)
{
	assert(way != NULL);
	assert(way->first_entity != NULL);
	
	if (way->first_entity == way->last_entity)
	{
		way->first_entity = NULL;
		way->last_entity = NULL;
	}
	else
	{
		Entity* succ = way->first_entity->succ_entity;
		
		way->first_entity = succ;
		succ->prec_entity = NULL;
	}
	
	way->active_entity_count--;
}

void way_notify_entity_starts_sleeping_in_light_queue(Way* way, Entity* entity)
{
	assert(way != NULL);
	assert(entity != NULL);
	assert(entity->state == STATE_SLEEPING);
	
	if (!entity->is_in_light_queue) // Entity is not already in the light queue
	{
		way->light_queue_size++;
		entity->is_in_light_queue = true;
	}
	
	way->active_entity_count--;
}

static void wake_up_entity_in_queue(Way* way, Entity* entity)
{
	entity_wake_up(entity);
	way->last_waken_up_queue_entity = entity;
	way->active_entity_count++;
}

void way_notify_entities_can_cross(Way* way)
{
	assert(way != NULL);
	
	Entity* waiting = way->next_entity_to_cross;
	
	// No waiting entity or the waiting entity has already been waken up
	if (waiting == NULL || waiting->state != STATE_SLEEPING)
		return;
	
	wake_up_entity_in_queue(way, waiting);
}

static void wake_up_next_entity_in_queue(Way* way)
{
	if (way->next_entity_to_cross == NULL)
		return;
	
	Entity* next;
		
	if (way->last_waken_up_queue_entity == NULL)
		next = way->next_entity_to_cross->succ_entity;
	else
		next = way->last_waken_up_queue_entity->succ_entity;
		
	if (next == NULL || next->state != STATE_SLEEPING)
		return;
	
	wake_up_entity_in_queue(way, next);
}

void way_update_next_entity_to_cross(Way* way)
{
	assert(way != NULL);
	
	Entity* next_to_cross = way->next_entity_to_cross;
	
	if (next_to_cross == NULL)
		return;
	
	if (entity_has_moved(next_to_cross))
	{
		if (next_to_cross->is_in_light_queue)
		{
			assert(way->last_waken_up_queue_entity != NULL);
			wake_up_next_entity_in_queue(way);
		}
		
		if (entity_has_passed_crossroad_start(next_to_cross))
		{
			Entity* succ = next_to_cross->succ_entity;
			
			way->next_entity_to_cross = succ;
			
			if (next_to_cross->is_in_light_queue) // If the entity was waiting in the light queue
			{
				way->light_queue_size--;
				next_to_cross->is_in_light_queue = false;
			}
			
			if (!entity_has_passed_crossroad_end(next_to_cross))
			{
				way->last_crossing_entity = next_to_cross;
				
				if (way->first_crossing_entity == NULL)
					way->first_crossing_entity = next_to_cross;
			}
		}
	}
}

void way_notify_entity_crossed(Way* way)
{
	assert(way != NULL);
	assert(way->first_crossing_entity != NULL);
	
	Entity* entity = way->first_crossing_entity;
	
	if (entity == way->last_crossing_entity)
	{
		way->first_crossing_entity = NULL;
		way->last_crossing_entity = NULL;
	}
	else
	{
		way->first_crossing_entity = entity->succ_entity;
	}
}

bool way_is_light_queue_empty(Way const* way)
{
	assert(way != NULL);
	return way->light_queue_size == 0;
}

bool way_crossed_seg_free_when_reached(Way const* way, float seg_end_x, float seg_distance, float velocity)
{
	// This function assumes that no other entity of this way will start crossing before the segment
	// is reached by the entity arriving from a perpendicular way (typically the case during a red 
	// or yellow light).
	
	assert(way != NULL);
	assert(seg_distance >= 0.0f);
	assert(velocity >= 0.0f);
	
	Entity* last_crossing = way->last_crossing_entity;
	
	if (last_crossing == NULL)
		return true;
	
	float seg_reached_in = seg_distance / velocity;
	float last_crossing_vel = last_crossing->velocity;
	float last_crossing_end_x = entity_get_end_x(last_crossing);
	
	return last_crossing_end_x + last_crossing_vel * seg_reached_in > seg_end_x;
}
